FROM node
MAINTAINER Yordan Daskalov <jordan@jordas.net>

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app
RUN yarn install
COPY . /usr/src/app

ENV NODE_ENV production

EXPOSE 80
CMD ["npm", "run", "prod"]

