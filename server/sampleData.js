import Type from './models/type';
import Category from './models/category';
import Language from './models/language';
import Service from './models/service';
import News from './models/news';
import User from './models/user';
import _ from 'lodash';

export function addDefaultNews() {
    News.find({})
        .exec((newsErr, news) => {
            if (news.length === 0) {
                Language.findOne({ tag: 'EN' })
                    .exec((langErr, en) => {
                        Category.findOne({})
                            .exec((catErr, cat) => {
                                Type.findOne({ category: cat._id })
                                    .exec((typeErr, type) => {
                                        const news = {
                                            slug: 'test-news',
                                            category: cat._id,
                                            type: type._id,
                                            language: en._id,
                                            youtube: '',
                                            title: 'Test News',
                                            content: 'Test News\n<p></p>\n<p>Test news content.</p>\n',
                                            tags: [{ name: 'Test News Tag', slug: 'test news tag' }],
                                            comments: [],
                                            description: 'Test News Description',
                                            countries: [],
                                        };
                                        News.create(news, (serviceErr) => {
                                            if (!serviceErr) {
                                                console.log('^_^ Sample News Loaded ! Ready to go ^_^ ');
                                            } else {
                                                console.log(serviceErr);
                                            }
                                        });
                                    });
                            });
                    });
            }
        });
}

export function addDefaultServices() {
    Service.find({})
        .exec((serviceErr, services) => {
            if (services.length === 0) {
                Language.findOne({ tag: 'EN' })
                    .exec((langErr, bg) => {
                        Category.findOne({})
                            .exec((catErr, cat) => {
                                Type.findOne({ category: cat._id })
                                    .exec((typeErr, type) => {
                                        const service = {
                                            category: cat._id,
                                            type: type._id,
                                            language: bg._id,
                                            location: 'Sofia, Bulgaria',
                                            tags: ['Test 1', 'Test 11'],
                                            settings: {
                                                isLimitOrderQuantity: false,
                                                isPayment: false,
                                                isChildrenService: false,
                                                isAdultService: false,
                                                isEndHour: false,
                                                isStartHour: false,
                                                isEndDate: false,
                                                isStartDate: false,
                                                isDateCalendar: false,
                                                isRecurring: false,
                                            },
                                            currency: 'EUR',
                                            content: '<h2>Test 1</h2>\n<p></p>\n<p>Test 1Test 1Test 1Test 1</p>\n',
                                            description: 'Test 1',
                                            place: {
                                                locality: 'Sofia',
                                                administrative: 'Sofia City Province',
                                                country: 'Bulgaria',
                                                latitude: '42.6977082',
                                                longitude: '23.321867500000053',
                                            },
                                            title: 'Test 1',
                                        };
                                        Service.create(service, (serviceCreateErr) => {
                                            if (!serviceCreateErr) {
                                                console.log('^_^ Sample Services Loaded ! Ready to go ^_^ ');
                                                addDefaultNews();
                                            } else {
                                                console.log(serviceCreateErr);
                                            }
                                        });
                                    });
                            });
                    });
            }
        });
}

export function addDefaultTypes() {
    Language.findOne({ tag: 'BG' })
        .exec((langErr, en) => {
            Category.find({})
                .exec((catErr, languages) => {
                    Type.find({})
                        .exec((typeErr, types) => {
                            console.log('Adding Default Types:', types);
                            if (types.length < 1) {
                                const tourism = _.find(languages, { name: 'Tourism' });
                                const caretakers = _.find(languages, { name: 'Caretakers' });
                                const type1 = new Type({
                                    name: 'Tours',
                                    category: tourism._id,
                                    slug: 'tours',
                                    translations: [{ language: en._id, translation: 'Туризъм' }],
                                });
                                const type2 = new Type({
                                    name: 'Caretakers',
                                    category: caretakers._id,
                                    slug: 'caretakers',
                                    translations: [{ language: en._id, translation: 'Гоедачи' }],
                                });

                                Type.create([type1, type2], (error) => {
                                    if (!error) {
                                        console.log('^_^ Sample Types Loaded ! Ready to go ^_^ ');
                                        addDefaultServices();
                                    } else {
                                        console.log(error);
                                    }
                                });
                            }
                        });
                });
        });
}

export function addDefaultCategories() {
    Language.findOne({ tag: 'BG' })
        .exec((langErr, bg) => {
            Category.find({})
                .exec((err2, cats) => {
                    // console.log('Adding Default Categories:', cats);
                    if (cats.length < 1) {
                        const cat1 = new Category({ name: 'Tourism', slug: 'tours', translations: [{ language: bg._id, translation: 'Туризъм' }] });
                        const cat2 = new Category({ name: 'Caretakers', slug: 'caretakers', translations: [{ language: bg._id, translation: 'Гледачи' }] });

                        Category.create([cat1, cat2], (error) => {
                            if (!error) {
                                console.log('^_^ Sample Categories Loaded ! Ready to go ^_^ ');
                                //
                                addDefaultTypes();
                            } else {
                                console.log(error);
                            }
                        });
                    }
                });
        });
}

export function addAdminUser() {
    Language.findOne({ tag: 'BG' })
        .exec((langErr, bg) => {
            User.find({ username: 'admin' })
                .exec((userErr, user) => {
                    if (!user || user.length < 1) {
                        const admin = new User({
                            provider: 'local',
                            username: 'admin',
                            roles: ['user', 'provider', 'admin'],
                            password: '!AgeWeb2018',
                            settings: { subscriptions: { userContact: false, services: false, orders: false, newsletter: false }, language: bg._id },
                            access: [],
                            phone: '',
                            profileImageURL: '/api/static/img/default-image.png',
                            email: 'admin@ageweb.care',
                            lastName: 'ROOT',
                            firstName: 'Admin',
                        });
                        admin.save((err, saved) => {
                            if (!err) {
                                console.log('^_^ Admin User Loaded ! Ready to go ^_^ ');
                                //
                                addDefaultCategories();
                            } else {
                                console.log(err);
                            }
                        });
                    } else {
                        addDefaultCategories();
                    }
                });
        });
}

export function addDefaultLanguages() {
    Language.count()
        .exec((err, count) => {
            if (count > 0) {
                addAdminUser();
                return;
            }

            console.log('Adding Default Categories:', count);
            const en = new Language({ name: 'English', tag: 'EN' });
            const bg = new Language({ name: 'Български', tag: 'BG' });
            const de = new Language({ name: 'Deutsch', tag: 'DE' });

            Language.create([en, bg, de], (error) => {
                if (!error) {
                    console.log('^_^ Sample Languages Loaded ! Ready to go ^_^ ');
                    addAdminUser();
                } else {
                    console.log(error);
                }
            });
        });
}

export function populateDB() {
    addDefaultLanguages();
}
