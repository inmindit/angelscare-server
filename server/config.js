const config = {
    mongoURL: process.env.MONGO_URL || 'mongodb://localhost:27017/ageweb',
    port: process.env.PORT || 8080,
    secure: {
        ssl: false,
        port: 443,
        privateKey: './certificates/private.pem',
        certificate: './certificates/public.pem',
    },
    app: {
        title: 'AgeWeb.care',
    },
    client: {
        dist: '../../angelscare-client/dist',
        public: '../../angelscare-client/src/public',
        index: './templates/index.html',
    },
    mailer: {
        from: 'admin@ageweb.care',
        options: {
            host: 'smtp.zoho.com',
            port: 465,
            secure: true,
            auth: {
                user: 'admin@ageweb.care',
                pass: '!Ageweb2018',
            },
        },
    },
    // SESSIONS
    sessionCookie: {
        // session expiration is set by default to 24 hours
        maxAge: 24 * (60 * 60 * 1000),
        // httpOnly flag makes sure the cookie is only accessed
        // through the HTTP protocol and not JS/browser
        httpOnly: true,
        // secure cookie should be turned to true to provide additional
        // layer of security so that the cookie is set only when working
        // in HTTPS mode.
        secure: false,
    },
    // sessionSecret should be changed for security measures and concerns
    sessionSecret: 'T1cK3tS.I0!*&t0pS3cReT*()sessionID666',
    // sessionKey is set to the generic sessionId key used by PHP applications
    // for obsecurity reasons
    sessionKey: 'sessionId',
    sessionCollection: 'sessions',
    // Lusca CSRF
    csrf: {
        csrf: false,
        csp: { /* Content Security Policy object */ },
        xframe: 'SAMEORIGIN',
        p3p: 'ABCDEF',
        xssProtection: true,
    },
    facebook: {
        clientID: process.env.FACEBOOK_ID || 'APP_ID',
        clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
        callbackURL: '/api/auth/facebook/callback',
    },
    google: {
        clientID: process.env.GOOGLE_ID || 'APP_ID',
        clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
        callbackURL: '/api/auth/google/callback',
    },
    paypal: {
        port: 5000,
        api: {
            host: 'api.sandbox.paypal.com',
            port: '',
            client_id: 'YOUR_CLIENT_ID',  // your paypal application client id
            client_secret: 'YOUR_CLIENT_SECRET', // your paypal application secret id
        },
    },
    uploads: {
        profileUpload: {
            dest: './static/uploads/users/',
            // Profile upload destination path
            limits: {
                fileSize: 5 * 1024 * 1024,    // Max file size in bytes (5 MB)
            },
        },
        serviceUpload: {
            dest: './static/uploads/services/',
            // Profile upload destination path
            limits: {
                fileSize: 5 * 1024 * 1024,    // Max file size in bytes (5 MB)
            },
        },
        newsUpload: {
            dest: './static/uploads/news/',
            // Profile upload destination path
            limits: {
                fileSize: 5 * 1024 * 1024,    // Max file size in bytes (5 MB)
            },
        },
        clubUpload: {
            dest: './static/uploads/clubs/',
            // Profile upload destination path
            limits: {
                fileSize: 5 * 1024 * 1024,    // Max file size in bytes (5 MB)
            },
        },
    },
};

export default config;
