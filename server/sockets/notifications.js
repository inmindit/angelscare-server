import _ from 'lodash';
import mongoose from 'mongoose';
import Notification from '../models/notification';

const Subscribers = [];
let emit = {};

/* Notification.post('save', function (notification, next) {
 emit(notification);
 Notification.find({}).exec(function (err, notifications) {
 console.log("ALL NOTIFICATIONS:", notifications);
 });
 next();
 }); */

// Create the chat configuration
export default function (io, socket) {
    // Send a chat messages to all connected sockets when a message is received
    socket.on('Notifications', (user) => {
        Subscribers.push(user);
        console.log('USER:', user);
        console.log('SUBSCRIBERS:', Subscribers);
    });
    // Emit the status event when a socket client is disconnected
    socket.on('disconnect', (user) => {
        _.remove(Subscribers, user);
        console.log('USER:', user);
        console.log('SUBSCRIBERS:', Subscribers);
    });

    //
    function EmitNotification(data) {
        if (_.isArray(data)) {
            _.each(data, (d) => {
                io.emit('notification', {
                    type: d.type,
                    title: d.title,
                    text: d.text,
                    created: d.created,
                    updated: d.updated,
                    status: d.status,
                    user: d.user,
                });
            });
        } else {
            io.emit('notification', {
                type: data.type,
                title: data.title,
                text: data.text,
                created: data.created,
                updated: data.updated,
                status: data.status,
                user: data.user,
            });
        }
    }

    emit = EmitNotification;
    emit({
        type: 'status',
        title: 'SERVER',
        text: 'You are now connected',
        created: Date.now(),
        updated: Date.now(),
        status: 'sent',
        user: null,
    });
}
