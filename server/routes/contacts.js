import * as Contact from '../controllers/contact';
import { Router } from 'express';

const router = new Router();

// Users collection routes
router.route('/emails/test')
    .post(Contact.testEmail);
router.route('/contact')
    .post(Contact.contactAdmin);
router.route('/contact/provider')
    .post(Contact.contactProvider);
router.route('/contact/club')
    .post(Contact.contactClub);

export default router;
