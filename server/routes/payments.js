import * as Payments from '../controllers/payments';
import Payment from '../models/payment';
import { Router } from 'express';

const router = new Router();
let sessionInfo;

router.post('/paypal/paynow', (req, res) => {
    sessionInfo = req.session;
    if (!sessionInfo.sessionData || sessionInfo.sessionData === '') {
        res.redirect('/');
        res.end();
    } else {
        const data = {
            userID: sessionInfo.sessionData.userID,
            data: req.body,
        };
        /*
        * call to paynow helper method to call paypal sdk
        */
        Payments.payNow(data, (error, result) => {
            if (error) {
                res.writeHead(200, { 'Content-Type': 'text/plain' });
                res.end(JSON.stringify(error));
            } else {
                sessionInfo.paypalData = result;
                sessionInfo.clientData = req.body;
                res.redirect(result.redirectUrl);
            }
        });
    }
});

/*
* payment success url
*/
router.get('/papal/execute', (req, res) => {
    sessionInfo = req.session;
    const PayerID = req.query.PayerID;
    if (!sessionInfo.sessionData || sessionInfo.sessionData === '') {
        res.redirect('/');
        res.end();
    } else {
        sessionInfo.state = 'success';
        Payments.getResponse(sessionInfo, PayerID, (response) => {
            res.render('executePayement', {
                response,
            });
        });
    }
});

/*
* payment cancel url
*/
router.get('/paypal/cancel', (req, res) => {
    sessionInfo = req.session;
    if (!sessionInfo.sessionData || sessionInfo.sessionData === '') {
        res.redirect('/');
        res.end();
    } else {
        const response = {};
        response.error = true;
        response.message = 'Payment unsuccessful.';
        response.userData = {
            name: sessionInfo.sessionData.name,
        };

        res.render('executePayement', {
            response,
        });
    }
});

export default router;
