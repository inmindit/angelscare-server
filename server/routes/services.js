import { Router } from 'express';
import * as Services from '../controllers/services';
import * as Categories from '../controllers/categories';
import * as Policies from '../policies/services';

const router = new Router();


// Services collection routes
router.route('/services')
    .all(Policies.isAllowed)
    .get(Services.list)
    .post(Services.create);

// Single service routes
router.route('/services/service/:serviceId')
    .all(Policies.isAllowed)
    .get(Services.read)
    .put(Services.update)
    .delete(Services.deleteService);


// Services Grouped by Category
router.route('/services/search')
    .all(Policies.isAllowed)
    .post(Services.searchServices);

// Services Grouped by Category
router.route('/services/current')
    .all(Policies.isAllowed)
    .get(Services.listCurrentByCategory);

// Services Grouped by Category
router.route('/services/category')
    .all(Policies.isAllowed)
    .get(Services.listAllByCategory);

// Services List for a single Category
router.route('/services/category/:categoryId')
    .all(Policies.isAllowed)
    .get(Services.listByCategory);

// Services List for a Provider.
router.route('/services/provider')
    .all(Policies.isAllowed)
    .get(Services.getProviderServices);

// Change service picture
router.route('/uploads/services/picture/:serviceId')
    .all(Policies.isAllowed)
    .post(Services.addServicePicture);
router.route('/uploads/services/picture/:serviceId/delete')
    .all(Policies.isAllowed)
    .post(Services.removeServicePicture);

// Finish by binding the service middleware
router.param('categoryId', Categories.categoryById);
router.param('serviceId', Services.serviceByID);

export default router;
