import { Router } from 'express';
import * as Languages from '../controllers/languages';
// import * as Policies from '../policies/languages';

const router = new Router();

// Get and add all Posts
router.route('/language')
    .get(Languages.getAllLanguages)
    .post(Languages.addLanguage);

// Get one post by cuid
router.route('/language/:languageId')
    .get(Languages.getLanguage)
    .put(Languages.updateLanguage)
    .delete(Languages.deleteLanguage);


router.param('languageId', Languages.languageByID);

export default router;
