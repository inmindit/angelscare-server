import * as Policy from '../policies/orders';
import * as Orders from '../controllers/orders';
import { Router } from 'express';

const router = new Router();

router.route('/orders/guest')
    .all(Policy.isAllowed)
    .post(Orders.createGuestOrder);

router.route('/orders/me')
    .all(Policy.isAllowed)
    .get(Orders.currentOrder)
    .post(Orders.createOrder);

router.route('/orders/me/history')
    .all(Policy.isAllowed)
    .get(Orders.orderHistory);

router.route('/orders/provider')
    .all(Policy.isAllowed)
    .get(Orders.orderProviderHistory);

router.route('/order/:orderId')
    .all(Policy.isAllowed)
    .get(Orders.orderByID)
    .delete(Orders.orderVoid);

router.route('/orders/transaction/:orderId')
    .all(Policy.isAllowed)
    .post(Orders.startPayment);

// Finish by binding the news middleware
router.param('orderId', Orders.orderByID);

export default router;
