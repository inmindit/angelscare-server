import * as AdminPolicy from '../policies/admin';
import * as Admin from '../controllers/admin';
import { Router } from 'express';

const router = new Router();

// Users collection routes
router.route('/admin/users')
    .all(AdminPolicy.isAllowed)
    .get(Admin.listUsers);

router.route('/admin/user/invite')
    .all(AdminPolicy.isAllowed)
    .post(Admin.inviteUser);
// Single user routes
router.route('/admin/users/:clientId')
    .all(AdminPolicy.isAllowed)
    .get(Admin.showUser)
    .put(Admin.updateUserRolesAndAccess)
    .delete(Admin.deleteUser);

router.route('/admin/orders')
    .get(Admin.getOrders);

router.route('/admin/contacts')
    .get(Admin.getContactRequests);

// Finish by binding the user middleware
router.param('clientId', Admin.userByID);

export default router;
