import { Router } from 'express';
import * as CategoryController from '../controllers/categories';
import * as TypeController from '../controllers/types';
import * as SearchController from '../controllers/search';

const router = new Router();

// Get and add all Posts
router.route('/search')
    .post(SearchController.search);

router.param('categoryId', CategoryController.categoryById);
router.param('typeId', TypeController.typeById);

export default router;
