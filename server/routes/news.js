import { Router } from 'express';
import * as NewsController from '../controllers/news';
import * as Policies from '../policies/news';

const router = new Router();

// Get all and add News
router.route('/news')
    .all(Policies.isAllowed)
    .get(NewsController.getAllNews)
    .post(NewsController.addNews);

// Get one article by cuid
router.route('/news/:newsId')
    .all(Policies.isAllowed)
    .get(NewsController.getNews)
    .put(NewsController.updateNews)
    .delete(NewsController.deleteNews);

// Get news by category or type
router.route('/news/category/:categoryId')
    .all(Policies.isAllowed)
    .get(NewsController.getNewsByCategory);
router.route('/news/type/:typeId')
    .all(Policies.isAllowed)
    .get(NewsController.getNewsByType);

// Change service picture
router.route('/uploads/news/picture/:newsId')
    .all(Policies.isAllowed)
    .post(NewsController.addNewsPicture);
router.route('/uploads/news/picture/:newsId/delete')
    .all(Policies.isAllowed)
    .post(NewsController.removeNewsPicture);

router.param('newsId', NewsController.newsByID);

export default router;
