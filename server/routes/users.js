import * as Users from '../controllers/users';
import { Router } from 'express';

const router = new Router();

// User Routes
// Setting up the users profile api

router.route('/users/me')
    .get(Users.Profile.me)
    .put(Users.Profile.update);
router.route('/users/me/setting')
    .post(Users.Profile.updateUserSetting);
router.route('/users/accounts')
    .delete(Users.Authentication.removeOAuthProvider);
router.route('/users/password')
    .post(Users.Password.changePassword);
router.route('/users/picture')
    .post(Users.Profile.changeProfilePicture);
// Finish by binding the user middleware
router.param('userId', Users.Authorization.userByID);

export default router;
