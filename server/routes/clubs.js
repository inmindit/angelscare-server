import { Router } from 'express';
import * as ClubsController from '../controllers/clubs';
import * as CategoryController from '../controllers/categories';
import * as TypeController from '../controllers/types';
import * as Policies from '../policies/clubs';

const router = new Router();

// Get and add all Posts
router.route('/clubs')
    .all(Policies.isAllowed)
    .get(ClubsController.getAllClubs)
    .post(ClubsController.addClub);

// Get one post by cuid
router.route('/clubs/:clubId')
    .all(Policies.isAllowed)
    .get(ClubsController.getClub)
    .put(ClubsController.updateClub)
    .delete(ClubsController.deleteClub);

router.route('/clubs/category/:categoryId')
    .all(Policies.isAllowed)
    .get(ClubsController.getClubsByCategory);
router.route('/clubs/type/:typeId')
    .all(Policies.isAllowed)
    .get(ClubsController.getClubsByType);

// Change service picture
router.route('/uploads/clubs/picture/:clubId')
    .all(Policies.isAllowed)
    .post(ClubsController.addClubPicture);
router.route('/uploads/clubs/picture/:clubId/delete')
    .all(Policies.isAllowed)
    .post(ClubsController.removeClubPicture);

router.param('clubId', ClubsController.clubByID);
router.param('categoryId', CategoryController.categoryById);
router.param('typeId', TypeController.typeById);

export default router;
