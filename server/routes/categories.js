import { Router } from 'express';
import * as CategoryController from '../controllers/categories';
import * as CategoryPolicy from '../policies/categories';

const router = new Router();

// Get and add all Posts
router.route('/categories')
    .all(CategoryPolicy.isAllowed)
    .get(CategoryController.getCategories)
    .post(CategoryController.saveCategories);

router.route('/categories/provider')
    .all(CategoryPolicy.isAllowed)
    .get(CategoryController.getProviderCategories);

router.route('/categories/category')
    .all(CategoryPolicy.isAllowed)
    .post(CategoryController.addCategory);

// Get one post by cuid
router.route('/categories/category/:categoryId')
    .all(CategoryPolicy.isAllowed)
    .put(CategoryController.updateCategory)
    .delete(CategoryController.deleteCategory);
router.route('/categories/category/:categoryId/translation')
    // .all(CategoryPolicy.isAllowed)
    .put(CategoryController.updateCategoryTranslations);

router.route('/categories/current')
    .all(CategoryPolicy.isAllowed)
    .get(CategoryController.getCurrentCategories);

router.route('/categories/save')
    .all(CategoryPolicy.isAllowed)
    .post(CategoryController.saveSettings);


router.param('categoryId', CategoryController.categoryById);

export default router;
