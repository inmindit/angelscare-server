import { Router } from 'express';
import * as Comments from '../controllers/comments';
import * as News from '../controllers/news';
import * as Services from '../controllers/services';

const router = new Router();

// Service Comments Routes
router.route('/service/:eventId/comment/:commentId')
    .get(Comments.getServiceComments)
    .post(Comments.addServiceComment)
    .put(Comments.updateComment)
    .delete(Comments.deleteComment);

// Posts Comments Routes
router.route('/news/:postId/comment/:commentId')
    .get(Comments.getPostComments)
    .post(Comments.addPostComment)
    .put(Comments.updateComment)
    .delete(Comments.deleteComment);

router.param('serviceId', Services.serviceByID);
router.param('newsId', News.newsByID);
router.param('commentId', Comments.commentById);

export default router;
