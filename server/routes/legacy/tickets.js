import * as Policy from '../policies/tickets';
import * as Tickets from '../controllers/tickets';
import * as Services from '../controllers/services';
import { Router } from 'express';

const router = new Router();

router.route('/eventTickets/ticketType/event/:eventId')
    .put(Tickets.changedTicketsType);
// Admin and manage Categories for an Service
router.route('/eventTickets/categories/:eventId')
    .all(Policy.isAllowed)
    .post(Tickets.categoriesPost)
    .get(Tickets.categoriesGet);
router.route('/eventTickets/categories/:eventId/:catId')
    .put(Tickets.categoriesPut)
    .delete(Tickets.categoriesDelete);
// Admin and manage Halls for an Service
router.route('/eventTickets/halls/:eventId')
    .all(Policy.isAllowed)
    .post(Tickets.hallsPost)
    .get(Tickets.hallsGet);
router.route('/eventTickets/halls/:eventId/:hallId')
    .put(Tickets.hallsPut)
    .delete(Tickets.hallsDelete);
// Admin and manage Seats for an Service
router.route('/eventTickets/seats/:eventId')
    .all(Policy.isAllowed)
    .post(Tickets.seatsPost)
    .get(Tickets.seatsGet);
router.route('/eventTickets/seats/:eventId/:seatId')
    .put(Tickets.seatsPut)
    .delete(Tickets.seatsDelete);
// Admin and manage Tickets for an Service
router.route('/eventTickets/tickets/:eventId/user/:userId/')
    .get(Tickets.orderUserHistoryGet);
// Finish by binding the news middleware
router.param('eventId', Services.serviceByID);

export default router;
