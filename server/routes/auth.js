/**
 * Module dependencies.
 */
import * as Users from '../controllers/users';
import { Router } from 'express';

const router = new Router();

// Setting up the Users password api
router.route('/auth/forgot')
    .post(Users.Password.forgot);
router.route('/auth/reset/:token')
    .get(Users.Password.validateResetToken);
router.route('/auth/reset/:token')
    .post(Users.Password.reset);
// Setting up the Users authentication api
router.route('/auth/signup')
    .post(Users.Authentication.signup);
router.route('/auth/signin')
    .post(Users.Authentication.signin);
router.route('/auth/signout')
    .get(Users.Authentication.signout);
// Setting the facebook oauth routes
router.route('/auth/facebook')
    .get(Users.Authentication.oauthCall('facebook', { scope: ['email'] }));
router.route('/auth/facebook/callback')
    .get(Users.Authentication.oauthCallback('facebook'));
// Setting the google oauth routes
router.route('/auth/google')
    .get(Users.Authentication.oauthCall('google', {
        scope: [
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email',
        ],
    }));
router.route('/auth/google/callback')
    .get(Users.Authentication.oauthCallback('google'));
// Setting the paypal oauth routes
router.route('/auth/paypal')
    .get(Users.Authentication.oauthCall('paypal'));
router.route('/auth/paypal/callback')
    .get(Users.Authentication.oauthCallback('paypal'));

export default router;

