import { Router } from 'express';
import * as TypeController from '../controllers/types';
import * as CategoryController from '../controllers/categories';
import * as TypePolicy from '../policies/types';

const router = new Router();

// Get and add all Posts
router.route('/types')
    .all(TypePolicy.isAllowed)
    .get(TypeController.getTypes)
    .post(TypeController.saveTypes);

router.route('/types/type')
    .all(TypePolicy.isAllowed)
    .post(TypeController.addType);

// Get one post by cuid
router.route('/types/type/:typeId')
    .all(TypePolicy.isAllowed)
    .put(TypeController.updateType)
    .delete(TypeController.deleteType);

router.route('/types/type/:typeId/translation')
// .all(CategoryPolicy.isAllowed)
    .put(TypeController.updateTypeTranslations);

router.route('/types/category/:categoryId')
    .all(TypePolicy.isAllowed)
    .get(TypeController.getCategoryTypes);

router.route('/types/current')
    .all(TypePolicy.isAllowed)
    .get(TypeController.getCurrentTypes);

router.param('categoryId', CategoryController.categoryById);
router.param('typeId', TypeController.typeById);

export default router;
