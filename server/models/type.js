import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const TypeSchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
    },
    name: {
        type: String,
        default: '',
        unique: true,
    },
    slug: {
        type: String,
        default: '',
        unique: true,
    },
    translations: [{
        language: {
            type: Schema.Types.ObjectId,
            ref: 'Language',
            autopopulate: true,
        },
        translation: {
            type: String,
        },
    }],
});
TypeSchema.index({
    language_override: 'languageOverride',
});
TypeSchema.plugin(Autopopulate);
export default mongoose.model('Type', TypeSchema);

