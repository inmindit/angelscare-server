/**
 * Module dependencies
 */
import mongoose from 'mongoose';

const crypto = require('crypto');
import validator from 'validator';
import PasswordGenerator from 'strict-password-generator';
import owasp from 'owasp-password-strength-test';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

/**
 * A Validation function for local strategy email
 */
const validateLocalStrategyEmail = function (email) {
    return ((this.provider !== 'local' && !this.updated) || validator.isEmail(email, { require_tld: false }));
};

/**
 * User Schema
 */
const UserSchema = new Schema({
    firstName: {
        type: String,
        trim: true,
        default: '',
    },
    lastName: {
        type: String,
        trim: true,
        default: '',
    },
    displayName: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        index: {
            unique: true,
            sparse: true, // For this to work on a previously indexed field, the index must be dropped & the application restarted.
        },
        lowercase: true,
        trim: true,
        default: '',
        validate: [validateLocalStrategyEmail, 'Please fill a valid email address'],
    },
    username: {
        type: String,
        unique: 'Username already exists',
        required: 'Please fill in a username',
        lowercase: true,
        trim: true,
    },
    profileImageURL: {
        type: String,
        default: '/api/static/img/default-image.png',
    },
    location: {
        type: String,
        trim: true,
    },
    fullLocation: {
        latitude: {
            type: String,
            trim: true,
        },
        longitude: {
            type: String,
            trim: true,
        },
        country: {
            type: String,
            trim: true,
        },
        administrative: {
            type: String,
            trim: true,
        },
        locality: {
            type: String,
            trim: true,
        },
        neighborhood: {
            type: String,
            trim: true,
        },
        route: {
            type: String,
            trim: true,
        },
        street: {
            type: String,
            trim: true,
        },
        postal: {
            type: String,
            trim: true,
        },
    },
    phone: {
        type: String,
        default: '',
        trim: true,
    },
    access: [{
        type: Schema.Types.ObjectId,
        ref: 'Category',
        autopopulate: true,
    }],
    settings: {
        language: {
            type: Schema.Types.ObjectId,
            ref: 'Language',
            autopopulate: true,
        },
        country: {
            type: String,
        },
        subscriptions: {
            newsletter: {
                type: Boolean,
                default: false,
            },
            orders: {
                type: Boolean,
                default: false,
            },
            services: {
                type: Boolean,
                default: false,
            },
            userContact: {
                type: Boolean,
                default: false,
            },
        },
    },
    password: {
        type: String,
        default: '',
    },
    salt: {
        type: String,
    },
    roles: {
        type: [{
            type: String,
            enum: [
                'user',
                'provider',
                'admin',
            ],
        }],
        default: ['user'],
        required: 'Please provide at least one role',
    },
    status: {
        type: String,
        enum: ['invited', 'registered'],
    },
    updated: {
        type: Date,
        default: Date.now,
    },
    created: {
        type: Date,
        default: Date.now,
    },
    resetPasswordToken: {
        type: String,
    },
    resetPasswordExpires: {
        type: Date,
    },
    provider: {
        type: String,
        required: 'Provider is required',
    },
    providerData: {},
    additionalProvidersData: {},
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function (next) {
    if (this.password && this.isModified('password')) {
        this.salt = crypto.randomBytes(16)
            .toString('base64');
        this.password = this.hashPassword(this.password);
    }

    next();
});


/**
 * Hook a pre validate method to test the local password
 */
UserSchema.pre('validate', function (next) {
    if (this.provider === 'local' && this.password && this.isModified('password')) {
        owasp.config({
            allowPassphrases: false,
            maxLength: 128,
            minLength: 6,
            minPhraseLength: 6,
            minOptionalTestsToPass: 2,
        });
        const result = owasp.test(this.password);
        if (result.errors.length) {
            const error = result.errors.join(' ');
            this.invalidate('password', error);
        }
    }

    next();
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function (password) {
    if (this.salt && password) {
        return crypto.pbkdf2Sync(password, new Buffer(this.salt, 'base64'), 10000, 64, 'sha512')
            .toString('base64');
    } else {
        return password;
    }
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function (username, suffix, callback) {
    const _this = this;
    const possibleUsername = username.toLowerCase() + (suffix || '');

    _this.findOne({
        username: possibleUsername,
    }, (err, user) => {
        if (!err) {
            if (!user) {
                callback(possibleUsername);
            } else {
                _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};

/**
 * Generates a random passphrase that passes the owasp test
 * Returns a promise that resolves with the generated passphrase, or rejects with an error if something goes wrong.
 * NOTE: Passphrases are only tested against the required owasp strength tests, and not the optional tests.
 */
UserSchema.statics.generateRandomPassphrase = function () {
    return new Promise((resolve, reject) => {
        let password = '';
        const repeatingCharacters = new RegExp('(.)\\1{2,}', 'g');

        // iterate until the we have a valid passphrase
        // NOTE: Should rarely iterate more than once, but we need this to ensure no repeating characters are present
        while (password.length < 20 || repeatingCharacters.test(password)) {
            // build the random password
            password = PasswordGenerator.generatePassword({
                exactLength: 16,
                number: true,
                specialCharacter: true,
            });

            // check if we need to remove any repeating characters
            password = password.replace(repeatingCharacters, '');
        }

        // Send the rejection back if the passphrase fails to pass the strength test
        if (owasp.test(password).errors.length) {
            reject(new Error('An unexpected problem occured while generating the random passphrase'));
        } else {
            // resolve with the validated passphrase
            resolve(password);
        }
    });
};

/* UserSchema.post('init', function (user) {
    //user.password = undefined;
    //user.salt = undefined;
    //user.resetPasswordToken = undefined;
    //user.resetPasswordExpires = undefined;
}); */
UserSchema.index({
    language_override: 'languageOverride',
});
UserSchema.plugin(autopopulate);
export default mongoose.model('User', UserSchema);
