import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        autopopulate: true,
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'Type',
        autopopulate: true,
    },
    title: {
        type: String,
        default: '',
        trim: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    date: {
        type: Date,
        default: Date.now(),
    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    },
    startHours: {
        type: String,
        default: '08:00',
    },
    endHours: {
        type: String,
        default: '18:00',
    },
    countries: [{ type: 'String' }],
    language: {
        type: Schema.Types.ObjectId,
        ref: 'Language',
        autopopulate: true,
    },
    location: {
        type: String,
        trim: true,
    },
    place: {
        latitude: {
            type: String,
            trim: true,
        },
        longitude: {
            type: String,
            trim: true,
        },
        country: {
            type: String,
            trim: true,
        },
        administrative: {
            type: String,
            trim: true,
        },
        locality: {
            type: String,
            trim: true,
        },
        neighborhood: {
            type: String,
            trim: true,
        },
        route: {
            type: String,
            trim: true,
        },
        street: {
            type: String,
            trim: true,
        },
        postal: {
            type: String,
            trim: true,
        },
    },
    contact: {
        type: String,
        trim: true,
    },
    phone: {
        type: String,
        trim: true,
    },
    description: {
        type: String,
        default: '',
        trim: true,
    },
    content: {
        type: String,
        default: '',
    },
    price: {
        type: Number,
        default: 0,
    },
    currency: {
        type: String,
        default: 'EUR',
    },
    orderQuantity: { type: Number, default: 0 },
    settings: {
        isRecurring: { type: Boolean, default: false }, // Service is available every day
        isDateCalendar: { type: Boolean, default: false },
        isStartDate: { type: Boolean, default: false },
        isEndDate: { type: Boolean, default: false },
        isStartHour: { type: Boolean, default: false },
        isEndHour: { type: Boolean, default: false },
        isAdultService: { type: Boolean, default: false },
        isChildrenService: { type: Boolean, default: false },
        isPayment: { type: Boolean, default: false },
        isLimitOrderQuantity: { type: Boolean, default: false },
    },
    tags: [{
        name: {
            type: String,
            trim: true,
        },
        slug: {
            type: String,
            trim: true,
        },
    }],
    slug: {
        type: String,
    },
    image: {
        type: String,
        default: '/api/static/img/default-image.png',
        trim: true,
    },
    status: {
        type: String,
        enum: [
            'draft',
            'published',
        ],
        default: 'draft',
    },
    fields: [{
        field: {
            type: String,
        },
        show: {
            type: Boolean,
        },
    }],
});
ServiceSchema.index({
    title: 'text',
    slug: 'text',
}, {
    language_override: 'languageOverride',
});

ServiceSchema.plugin(autopopulate);
export default mongoose.model('Service', ServiceSchema);

