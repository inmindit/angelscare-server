import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const Notifications = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
        default: Date.now,
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
    },
    title: {
        type: String,
        trim: true,
    },
    text: {
        type: String,
    },
    type: {
        type: String,
        trim: true,
    },
    status: {
        type: String,
        enum: ['sent', 'received', 'seen'],
    },
});
Notifications.plugin(Autopopulate);
export default mongoose.model('Notification', Notifications);
