import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const LanguageSchema = new Schema({
    tag: {
        type: 'String',
        required: true,
    },
    name: {
        type: 'String',
        required: true,
        unique: true,
    },
    countries: [{ type: 'String' }],
    json: { type: 'String' },
});
export default mongoose.model('Language', LanguageSchema);
