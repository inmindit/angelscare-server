import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;


const FieldSchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
        default: Date.now,
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
    },
    name: {
        type: String,
        default: '',
        trim: true,
    },
    type: {
        type: String,
        default: '',
        trim: true,
    },
    child: {
        type: Schema.ObjectId,
        ref: 'Field',
    },
});
FieldSchema.plugin(Autopopulate);

export default mongoose.model('Field', FieldSchema);
