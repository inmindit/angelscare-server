import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;


const SeatSchema = new Schema({
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
        autopopulate: true,
    },
    row: {
        type: Number,
    },
    seat: {
        type: Number,
    },
    number: {
        type: Number,
    },
    hall: {
        type: Schema.Types.ObjectId,
        ref: 'TicketHall',
        autopopulate: true,
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'TicketCategory',
        autopopulate: true,
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'Order',
        autopopulate: true,
    },
    divided: {
        type: Boolean,
        default: false,
    },
    state: {
        type: String,
        enum: [
            'free',
            'reserved',
            'sold',
            'checkedin',
        ],
        default: 'free',
    },
    created: {
        type: Date,
        default: Date.now,
    },
});

SeatSchema.pre('remove', function (next, done) {
    console.log('SEAT PRE REMOVE: ', this._id);
    const seat = this;
    console.log('SEAT:', seat);
    console.log('STATE:', seat.state);
    if (seat.state === 'reserved' || seat.state === 'sold' || seat.state === 'checkedin') {
        console.log('Не може да се изтрива заето или продадено място !');
        done(new Error('Не може да се изтрива заето или продадено място !'));
    } else {
        next();
    }
});
SeatSchema.pre('save', function (next) {
    if (this.isNew) {
        this.updated = Date.now();
    }
    return next();
});

SeatSchema.plugin(Autopopulate);

export default mongoose.model('Seat', SeatSchema);
