import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';
const Schema = mongoose.Schema;

const TicketHallSchema = new Schema({
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
    },
    name: {
        type: String,
        trim: true,
        unique: true,
        dropDups: true,
    },
    description: {
        type: String,
        trim: true,
    },
    order: {
        type: Number,
        default: 1,
    },
    created: {
        type: Date,
        default: Date.now,
    },
});

TicketHallSchema.pre('save', function (next) {
    if (this.isNew) {
        this.updated = Date.now();
    }
    return next();
});
TicketHallSchema.plugin(Autopopulate);
export default mongoose.model('TicketHall', TicketHallSchema);
