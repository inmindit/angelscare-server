import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;


const TicketCategorySchema = new Schema({
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
    },
    name: {
        type: String,
        default: '',
    },
    description: {
        type: String,
        trim: true,
    },
    price: {
        type: Number,
        default: 0,
    },
    order: {
        type: Number,
        default: 1,
    },
    seats: {
        type: Number,
        default: 0,
    },
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
        default: Date.now,
    },
    color: {
        type: String,
        default: '#464545',
        enum: ['#008000', '#00ff00', '#66ff66', '#330080', '#6600ff', '#a366ff'],
    },
});

TicketCategorySchema.pre('save', function (next) {
    if (this.isNew) {
        this.updated = Date.now();
    }
    return next();
});
TicketCategorySchema.plugin(Autopopulate);
export default mongoose.model('TicketCategory', TicketCategorySchema);
