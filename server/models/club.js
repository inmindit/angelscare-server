import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const ClubSchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    category: { type: Schema.Types.ObjectId, ref: 'Category', autopopulate: true },
    type: { type: Schema.Types.ObjectId, ref: 'Type', autopopulate: true },
    title: {
        type: String,
        default: '',
        trim: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    date: {
        type: Date,
        default: Date.now(),
    },
    workingHours: {
        start: {
            type: String,
            default: '08:00',
        },
        end: {
            type: String,
            default: '18:00',
        },
    },
    language: {
        type: Schema.Types.ObjectId,
        ref: 'Language',
        autopopulate: true,
    },
    location: {
        type: String,
        trim: true,
    },
    place: {
        latitude: {
            type: String,
            trim: true,
        },
        longitude: {
            type: String,
            trim: true,
        },
        country: {
            type: String,
            trim: true,
        },
        city: {
            type: String,
            trim: true,
        },
        area: {
            type: String,
            trim: true,
        },
        address: {
            type: String,
            trim: true,
        },
        zip: {
            type: String,
            trim: true,
        },
    },
    contact: {
        type: String,
        trim: true,
    },
    attendant: {
        type: String,
        trim: true,
    },
    phone: {
        type: String,
        trim: true,
    },
    description: {
        type: String,
        default: '',
        trim: true,
    },
    image: {
        type: String,
        default: '/api/static/img/default-image.png',
        trim: true,
    },
    events: [{
        description: { type: String },
        date: { type: Date },
    }],
    services: [{
        type: Schema.Types.ObjectId,
        ref: 'Service',
        autopopulate: true,
    }],
});
ClubSchema.index({
    title: 'text',
    location: 'text',
    description: 'text',
}, {
    language_override: 'languageOverride',
});
ClubSchema.plugin(autopopulate);
export default mongoose.model('Club', ClubSchema);

