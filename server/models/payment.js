/**
 * Module dependencies.
 */
import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const PaymentSchema = new Schema({
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
        autopopulate: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        autopopulate: true,
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'Order',
        autopopulate: true,
    },
    paypalData: {
        intent: {
            type: String,
            default: 'authorize',
        },
        payer: {
            payment_method: {
                type: String,
                default: 'paypal',
            },
        },
        redirect_urls: {
            return_url: {
                type: String,
                default: 'http://127.0.0.1:81/api/payment/execute',
            },
            cancel_url: {
                type: String,
                default: 'http://127.0.0.1:81/api/payment/cancel',
            },
        },
        transactions: [{
            amount: {
                total: {
                    type: Schema.Types.Number,
                    default: 0,
                },
                currency: {
                    type: String,
                    default: 'USD',
                },
            },
            description: {
                type: String,
                default: 'PayPal payment description',
            },
        }],
    },
    status: {
        type: String,
        enum: [
            'initialized',
            'in-progress',
            'success',
            'error',
            'void',
        ],
        default: 'initialized',
    },
    updated: {
        type: Schema.Types.Date,
        default: Date.now,
    },
    created: {
        type: Schema.Types.Date,
        default: Date.now,
    },
});
PaymentSchema.plugin(Autopopulate);
export default mongoose.model('Payment', PaymentSchema);
