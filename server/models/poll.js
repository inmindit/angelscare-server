import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;
/**
 *  Poll Schema
 */
const ProfilePollSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    age: {
        type: Number,
        default: '',
    },
    gender: {
        type: String,
        enum: [
            'Male',
            'Female',
            'Other',
            'none',
        ],
        default: 'none',
    },
    profession: {
        type: String,
        default: '',
    },
    interests: [{
        type: String,
        default: '',
    }],
    cities: {
        type: String,
        default: '',
    }
});
ProfilePollSchema.plugin(Autopopulate);

export default mongoose.model('Poll', ProfilePollSchema);
