import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const ContactSchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
    },
    club: {
        type: Schema.Types.ObjectId,
        ref: 'Club',
    },
    // for anonymous requests
    email: {
        type: String,
    },
    provider: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    subject: {
        type: String,
        default: '',
    },
    message: {
        type: String,
        default: '',
    },
    details: {
        yearOfBirth: {
            type: String,
        },
        streetAddress: {
            type: String,
        },
        postalCode: {
            type: String,
        },
    },
    reason: {
        type: String,
        default: 'Contact Inquiry',
    },
});
ContactSchema.index({
    language_override: 'languageOverride',
});
ContactSchema.plugin(Autopopulate);
export default mongoose.model('Contact', ContactSchema);

