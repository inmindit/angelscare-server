import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const OrdersSchema = new Schema({
    service: {
        type: Schema.Types.ObjectId,
        ref: 'Service',
        autopopulate: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        autopopulate: { select: 'displayName firstName lastName email username profileImageURL location settings roles updated access' },
    },
    state: {
        type: String,
        enum: [
            'in-progress',
            'in-transaction',
            'success',
            'error',
            'void',
        ],
        default: 'in-progress',
    },
    quantity: {
        type: Number,
        default: 1,
    },
    updated: {
        type: Date,
        default: Date.now,
    },
    created: {
        type: Date,
        default: Date.now,
    },
}, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
});
OrdersSchema.index({
    user: 1,
    state: { 'in-progress': 1 },
}, { unique: true });

OrdersSchema.pre('save', function (next) {
    if (this.isNew) {
        this.updated = Date.now();
    }
    return next();
});
OrdersSchema.pre('remove', (next) => {
    // console.log('ORDER PRE REMOVE EVENT !');
});
OrdersSchema.plugin(Autopopulate);
export default mongoose.model('Order', OrdersSchema);
