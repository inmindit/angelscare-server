import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const NewsSchema = new Schema({
    title: { type: 'String', required: true },
    content: { type: 'String', required: true },
    slug: { type: 'String', required: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category', autopopulate: true },
    type: { type: Schema.Types.ObjectId, ref: 'Type', autopopulate: true },
    location: { type: 'String' },
    countries: [{ type: 'String' }],
    language: {
        type: Schema.Types.ObjectId,
        ref: 'Language',
        autopopulate: true,
    },
    description: {
        type: String,
        default: '',
        trim: true,
    },
    created: { type: 'Date', default: Date.now, required: true },
    updated: { type: 'Date', default: Date.now },
    comments: [{ type: Schema.Types.ObjectId, ref: 'Comment', autopopulate: true }],
    tags: [{
        name: {
            type: String,
            trim: true,
        },
        slug: {
            type: String,
            trim: true,
        },
    }],
    youtube: {
        type: String,
    },
    image: {
        type: String,
        default: '/api/static/img/default-image.png',
        trim: true,
    },
    user: { type: Schema.ObjectId, ref: 'User' },
    settings: {

    },
});
NewsSchema.index({
    title: 'text',
    content: 'text',
    slug: 'text',
    location: 'text',
}, {
    language_override: 'languageOverride',
});
NewsSchema.plugin(autopopulate);
export default mongoose.model('News', NewsSchema);
