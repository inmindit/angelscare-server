import mongoose from 'mongoose';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;
import comments from './comment';

const SettingsSchema = new Schema({

});
SettingsSchema.plugin(autopopulate);
export default mongoose.model('Settings', SettingsSchema);
