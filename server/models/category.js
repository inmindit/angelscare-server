import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    name: {
        type: String,
        default: '',
        unique: true,
    },
    slug: {
        type: String,
        default: '',
        unique: true,
    },
    color: {
        type: String,
    },
    translations: [{
        language: {
            type: Schema.Types.ObjectId,
            ref: 'Language',
            autopopulate: true,
        },
        translation: {
            type: String,
        },
    }],
});
CategorySchema.index({
    language_override: 'languageOverride',
});
CategorySchema.plugin(Autopopulate);
export default mongoose.model('Category', CategorySchema);

