import mongoose from 'mongoose';
import Autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const Comments = new Schema({
    created: {
        type: Date,
        default: Date.now,
    },
    updated: {
        type: Date,
        default: Date.now,
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
    },
    text: {
        type: String,
        default: '',
        trim: true,
    },
    parent: {
        type: String,
        parent: { type: Schema.ObjectId, refPath: 'parent.type' },
        // autopopulate: true,
    },
});
Comments.plugin(Autopopulate);
export default mongoose.model('Comment', Comments);
