/**
 * Module dependencies
 */
const LocalStrategy = require('passport-local').Strategy;
import User from '../../models/user';


export default function (passport) {
    // Use local strategy
    passport.use(new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password'
        },
        (username, password, done) => {
            // console.log("LOCAL AUTH: "+username, password);
            User.findOne({
                username: username.toLowerCase()
            }, (err, user) => {
                if (err) {
                    return done(err);
                }
                if (!user || !user.authenticate(password)) {
                    return done(null, false, {
                        error: 'Invalid username or password'
                    });
                }

                done(null, user);
            });
        }));
}
