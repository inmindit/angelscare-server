/**
 * Module dependencies
 */
import FacebookStrategy from 'passport-facebook';
import { Authentication } from '../../controllers/users';
import config from '../../config';

export default function (passport) {
    // Use facebook strategy
    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL,
            profileFields: ['id', 'name', 'displayName', 'emails', 'photos'],
            passReqToCallback: true,
        },
        (req, accessToken, refreshToken, profile, done) => {
            // Set the provider data and include tokens
            const providerData = profile._json;
            providerData.accessToken = accessToken;
            providerData.refreshToken = refreshToken;

            function generateUsername(userProfile) {
                let username = '';

                if (userProfile.emails) {
                    username = userProfile.emails[0].value.split('@')[0];
                } else if (userProfile.name) {
                    username = userProfile.name.givenName[0] + userProfile.name.familyName;
                }

                return username.toLowerCase() || undefined;
            }

            // Create the user OAuth profile
            const providerUserProfile = {
                firstName: profile.name.givenName,
                lastName: profile.name.familyName,
                displayName: profile.displayName,
                email: profile.emails ? profile.emails[0].value : undefined,
                username: profile.username || generateUsername(profile),
                profileImageURL: (profile.id) ? `//graph.facebook.com/${profile.id}/picture?type=large` : undefined,
                provider: 'facebook',
                providerIdentifierField: 'id',
                providerData,
            };

            // Save the user OAuth profile
            Authentication.saveOAuthUserProfile(req, providerUserProfile, done);
        }));
}
