/**
 * Module dependencies
 */
import passport from 'passport';
import User from '../models/user';

import facebook from './strategies/facebook';
import google from './strategies/google';
import local from './strategies/local';

/**
 * Module init function
 */
export default function () {
    // Serialize sessions
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    // Deserialize sessions
    passport.deserializeUser((id, done) => {
        // console.log('Deserialize UserID: '+id);
        User.findOne({
            _id: id,
        }, '-salt -password', (err, user) => {
            done(err, user);
        });
    });

    // Add passport's middleware
    facebook(passport);
    google(passport);
    local(passport);
}
