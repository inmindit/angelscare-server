import Express from 'express';
import http from 'http';
import https from 'https';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import fs from 'fs';

const MongoStore = require('connect-mongo')(session);
import helmet from 'helmet';
import lusca from 'lusca';
import compression from 'compression';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import passport from 'passport';
import path from 'path';
import authConfig from './auth/auth';

// Webpack Requirements
import webpack from 'webpack';
import config from '../webpack.config.server';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

// Initialize the Express App
const app = new Express();

// Set Development modes checks
const isDevMode = process.env.NODE_ENV === 'development' || false;
const isProdMode = process.env.NODE_ENV === 'production' || false;

// Run Webpack dev server in development mode
if (isDevMode) {
    const compiler = webpack(config);
    app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
    app.use(webpackHotMiddleware(compiler));
}

// Import required modules
import admin from './routes/admin';
import auth from './routes/auth';
import comments from './routes/comments';
import categories from './routes/categories';
import languages from './routes/languages';
import types from './routes/types';
import services from './routes/services';
import orders from './routes/orders';
import payments from './routes/payments';
import news from './routes/news';
import clubs from './routes/clubs';
import users from './routes/users';
import search from './routes/search';
import contacts from './routes/contacts';

import * as sampleData from './sampleData';

import serverConfig from './config';
import swig from 'swig-templates';

// Set native promises as mongoose promise
mongoose.Promise = global.Promise;

// MongoDB Connection
mongoose.connect(serverConfig.mongoURL, (error) => {
    if (error) {
        console.error('Please make sure Mongodb is installed and running and oyu have the proper credentials set to the databases!'); // eslint-disable-line no-console
        throw error;
    }

    // feed some dummy data in DB.
    sampleData.populateDB();
});

// Enable CORS
app.use((req, res, next) => {
    const allowedOrigins = [
        'localhost',
        '127.0.0.1',
        'ageweb.care',
        'ageweb.eu',
        'ageweb.net',
        'ageweb.org',
        'ageweb.info',
        'ageweb.tech',
        'care-angels.care',
        'care-angels.tech',
        'care-angels.at',
        'pflege-engel.care',
        'pflege-engel.at',
        'pflege-engel.org',
        '18.184.177.91',
        'ec2-18-184-177-91.eu-central-1.compute.amazonaws.com',
    ];
    const origin = req.headers.origin;
    allowedOrigins.forEach((value) => {
        if (value.indexOf(origin) > -1) {
            res.setHeader('Access-Control-Allow-Origin', origin);
        }
    });
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, X-Api-Key, Accept');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', 'true');

    // Pass to next layer of middleware
    if (req.method === 'OPTIONS') {
        res.status(200);
    } else {
        next();
    }
});

app.use(compression());
app.use(cookieParser(serverConfig.sessionSecret));

// Init SESSION with MongoDB session storage
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: serverConfig.sessionSecret,
    cookie: {
        maxAge: serverConfig.sessionCookie.maxAge,
        httpOnly: serverConfig.sessionCookie.httpOnly,
        secure: serverConfig.sessionCookie.secure && serverConfig.secure.ssl,
    },
    key: serverConfig.sessionKey,
    store: new MongoStore({
        mongooseConnection: mongoose.connection,
        collection: serverConfig.sessionCollection,
    }),
}));
// Add Lusca CSRF Middleware
app.use(lusca(config.csrf));

// HELMET SECURITY
app.use(helmet());
app.use(helmet.xssFilter());
app.use(helmet.hsts({
    maxAge: serverConfig.sessionCookie.maxAge,
    includeSubdomains: true,
    force: true,
}));
app.disable('x-powered-by');

// Passport API
authConfig(app);
app.use(passport.initialize());
app.use(passport.session());

// Apply body Parser and server public assets and routes
app.use('/api', bodyParser.json({ limit: '20mb' }));
app.use('/api', bodyParser.urlencoded({ limit: '20mb', extended: false }));
app.use('/api/static', Express.static(path.resolve(__dirname, '../static')));
app.use('/api', auth);
app.use('/api', users);
app.use('/api', admin);
app.use('/api', languages);
app.use('/api', categories);
app.use('/api', types);
app.use('/api', services);
app.use('/api', news);
app.use('/api', clubs);
app.use('/api', orders);
app.use('/api', payments);
app.use('/api', search);
app.use('/api', comments);

// Email Testing
app.use('/api', contacts);

// Http and HTTPs Responses
// Client Serving
const redirectPort = isDevMode ? `:${serverConfig.port}` : `:${serverConfig.secure.port}`;

app.use('/', Express.static(path.resolve(__dirname, serverConfig.client.dist)));
app.use('/dist', Express.static(path.resolve(__dirname, serverConfig.client.dist)));
app.use('/src/public', Express.static(path.resolve(__dirname, serverConfig.client.public)));
app.use('*', (req, res) => {
    // res.sendFile(path.resolve(__dirname, './templates/index.html'));
    const template = swig.compileFile(path.resolve(__dirname, serverConfig.client.index));
    const host = req.headers.host.split(':')[0] || req.headers.hostname.split(':')[0];
    const indexHTML = template({
        path: `https://${host}${redirectPort}`,
    });
    res.send(indexHTML);
});

const httpServer = http.createServer((req, res) => {
    console.log(req.headers.host, req.headers.url);
    const host = req.headers.host.split(':')[0] || req.headers.hostname.split(':')[0];
    res.writeHead(301, { Location: `https://${host}${redirectPort}${req.url}` });
    res.end();
});

const privateKey = fs.readFileSync(serverConfig.secure.privateKey);
const certificate = fs.readFileSync(serverConfig.secure.certificate);
const httpsServer = https.createServer({
    key: privateKey,
    cert: certificate,
}, app);

if (isDevMode) {
    // httpServer.listen(80);
    httpsServer.listen(serverConfig.port, '127.0.0.1', (error) => {
        if (!error) {
            console.log(`TICKETS.IO DEV Client is running on port: 8080 @ 127.0.0.1 ! https://localhost:8080/ `); // eslint-disable-line
            console.log(`TICKETS.IO DEV Server is running on port: 8080 @ 127.0.0.1 ! https://localhost:8080/api `); // eslint-disable-line
        } else {
            console.log(error);
        }
    });
} else if (isProdMode) {
    /* httpServer.listen(80, '0.0.0.0', (error) => {
        if (!error) {
            console.log(`TICKETS.IO DEV HTTP -> HTPPS REDIRECT is running on port: 80 @ 0.0.0.0 ! http://ageweb.care/ -> https://ageweb.care/ `); // eslint-disable-line
        } else {
            console.log(error);
        }
    }); */
    httpsServer.listen(serverConfig.secure.port, '0.0.0.0', (error) => {
            if (!error) {
                console.log(`TICKETS.IO PROD Client is running on port: 443 @ 0.0.0.0 ! https://ageweb.care/ `); // eslint-disable-line
                console.log(`TICKETS.IO PROD Server is running on port: 443 @ 0.0.0.0 ! https://ageweb.care/api `); // eslint-disable-line
            } else {
                console.log(error);
            }
        });
}

export default app;
