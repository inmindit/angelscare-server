import Acl from 'acl';

// Using the memory backend
const orderAcl = new Acl(new Acl.memoryBackend());
/**
 * Invoke news Permissions
 */
orderAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/types',
                permissions: ['*'],
            },
            {
                resources: '/types/current',
                permissions: ['*'],
            },
            {
                resources: '/types/type',
                permissions: ['*'],
            },
            {
                resources: '/types/type/:typeId',
                permissions: ['*'],
            },
            {
                resources: '/types/category/:categoryId',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['provider','user','guest'],
        allows: [
            {
                resources: '/types',
                permissions: ['get'],
            },
            {
                resources: '/types/current',
                permissions: ['get'],
            },
            {
                resources: '/types/type',
                permissions: [],
            },
            {
                resources: '/types/type/:typeId',
                permissions: [],
            },
            {
                resources: '/types/category/:categoryId',
                permissions: ['get'],
            },
        ],
    },
]);

/**
 * Check If news Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = req.user ? req.user.roles : ['guest'];
    // Check for user roles

    orderAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({error: 'User Policy Authentication '});
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({error: 'User does not have proper rights to this API Route.'});
            }
        }
    });
}
