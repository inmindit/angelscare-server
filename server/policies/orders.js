import Acl from 'acl';

// Using the memory backend
const orderAcl = new Acl(new Acl.memoryBackend());
/**
 * Invoke news Permissions
 */
orderAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/order/:orderId',
                permissions: ['*'],
            },
            {
                resources: '/orders/me',
                permissions: ['*'],
            },
            {
                resources: '/orders/me/history',
                permissions: ['*'],
            },
            {
                resources: '/orders/provider',
                permissions: ['*'],
            },
            {
                resources: '/orders/guest',
                permissions: [],
            },
            {
                resources: '/orders/transaction/:orderId',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['provider'],
        allows: [
            {
                resources: '/order/:orderId',
                permissions: ['*'],
            },
            {
                resources: '/orders/me',
                permissions: ['*'],
            },
            {
                resources: '/orders/me/history',
                permissions: ['*'],
            },
            {
                resources: '/orders/provider',
                permissions: ['*'],
            },
            {
                resources: '/orders/guest',
                permissions: [],
            },
            {
                resources: '/orders/transaction/:orderId',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['user'],
        allows: [
            {
                resources: '/order/:orderId',
                permissions: ['*'],
            },
            {
                resources: '/orders/me',
                permissions: ['*'],
            },
            {
                resources: '/orders/me/history',
                permissions: ['*'],
            },
            {
                resources: '/orders/provider',
                permissions: [],
            },
            {
                resources: '/orders/guest',
                permissions: [],
            },
            {
                resources: '/orders/transaction/:orderId',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['guest'],
        allows: [
            {
                resources: '/order/:orderId',
                permissions: [],
            },
            {
                resources: '/orders/me',
                permissions: [],
            },
            {
                resources: '/orders/history',
                permissions: [],
            },
            {
                resources: '/orders/guest',
                permissions: ['post'],
            },
            {
                resources: '/orders/transaction/:orderId',
                permissions: [],
            },
        ]
    }
]);

/**
 * Check If news Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = req.user ? req.user.roles : ['guest'];
    // If an news is being processed and the current user created it then allow any manipulation
    if (req.order && req.user && req.order.user.id === req.user.id) {
        return next();
    }
    // Check for user roles
    orderAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({error: 'User Policy Authentication '});
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({error: 'User does not have proper rights to this API Route.'});
            }
        }
    });
}
