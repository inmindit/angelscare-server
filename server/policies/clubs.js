import Acl from 'acl';

// Using the memory backend
const clubsAcl = new Acl(new Acl.memoryBackend());

/**
 * Invoke clubs Permissions
 */
clubsAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/clubs',
                permissions: ['*'],
            },
            {
                resources: '/clubs/:clubId',
                permissions: ['*'],
            },
            {
                resources: '/uploads/clubs/picture/:clubId',
                permissions: ['*'],
            },
            {
                resources: '/uploads/clubs/picture/:clubId/delete',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['provider', 'user', 'guest'],
        allows: [
            {
                resources: '/clubs',
                permissions: ['get'],
            },
            {
                resources: '/clubs/:clubId',
                permissions: ['get'],
            },
        ],
    },
]);

/**
 * Check If clubs Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = req.user ? req.user.roles : ['guest'];
    // If an clubs is being processed and the current user created it then allow any manipulation
    if (req.club && req.user && req.club.user && (req.club.user.id === req.user.id || req.user.roles.indexOf('admin') > -1)) {
        return next();
    }
    // Check for user roles
    clubsAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({ error: 'User Policy Authentication ' });
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({ error: 'User does not have proper rights to this API Route.' });
            }
        }
    });
}
