import Acl from 'acl';

// Using the memory backend
const orderAcl = new Acl(new Acl.memoryBackend());
/**
 * Invoke news Permissions
 */
orderAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/categories',
                permissions: ['*'],
            },
            {
                resources: '/categories/current',
                permissions: ['*'],
            },
            {
                resources: '/categories/category',
                permissions: ['*'],
            },
            {
                resources: '/categories/category/:categoryId',
                permissions: ['*'],
            },
            {
                resources: '/categories/provider',
                permissions: ['*'],
            },
            {
                resources: '/categories/save',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['provider'],
        allows: [
            {
                resources: '/categories/provider',
                permissions: ['get'],
            },
        ]
    },
    {
        roles: ['provider', 'user', 'guest'],
        allows: [
            {
                resources: '/categories',
                permissions: ['get'],
            },
            {
                resources: '/categories/current',
                permissions: ['get'],
            },
            {
                resources: '/categories/category',
                permissions: [],
            }
            ,{
                resources: '/categories/category/:categoryId',
                permissions: [],
            },
        ],
    },
]);

/**
 * Check If news Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = req.user ? req.user.roles : ['guest'];

    // Check for user roles
    orderAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({error: 'User Policy Authentication '});
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({error: 'User does not have proper rights to this API Route.'});
            }
        }
    });
}
