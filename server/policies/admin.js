/**
 * Module dependencies
 */
import Acl from 'acl';

// Using the memory backend
const adminAcl = new Acl(new Acl.memoryBackend());

/**
 * Invoke Admin Permissions
 */
adminAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/admin/users',
                permissions: ['*'],
            },
            {
                resources: '/admin/users/:clientId',
                permissions: ['*'],
            },
            {
                resources: '/admin/user/invite',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: [
            'user',
            'provider',
            'guest',
        ],
        allows: [
            {
                resources: '/admin/users',
                permissions: [],
            },
            {
                resources: '/admin/users/:clientId',
                permissions: [],
            },
            {
                resources: '/admin/user/invite',
                permissions: [],
            },
        ],
    },
]);

/**
 * Check If Admin Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = (req.user) ? req.user.roles : ['guest'];

    // Check for user roles
    // console.log("ADMIN ACL: ",roles, req.route.path, req.method.toLowerCase());
    adminAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({ error: 'User Policy Authentication ' });
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({ error: 'User does not have proper rights to this API Route.' });
            }
        }
    });
}
