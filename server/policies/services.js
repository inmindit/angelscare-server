import Acl from 'acl';

// Using the memory backend
const servicesAcl = new Acl(new Acl.memoryBackend());

/**
 * Invoke news Permissions
 */
servicesAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/services',
                permissions: '*',
            },
            {
                resources: '/services/service/:serviceId',
                permissions: '*',
            },
            {
                resources: '/services/category',
                permissions: '*',
            },
            {
                resources: '/services/category/:categoryId',
                permissions: '*',
            },
            {
                resources: '/services/current',
                permissions: '*',
            },
            {
                resources: '/services/search',
                permissions: ['*'],
            },
            {
                resources: '/services/provider',
                permissions: '*',
            },
            {
                resources: '/uploads/services/picture/:serviceId',
                permissions: '*',
            },
            {
                resources: '/uploads/services/picture/:serviceId/delete',
                permissions: '*',
            },
        ],
    },
    {
        roles: ['provider'],
        allows: [
            {
                resources: '/services',
                permissions: ['get', 'post'],
            },
            {
                resources: '/services/service/:serviceId',
                permissions: ['get', 'put', 'delete'],
            },
            {
                resources: '/services/category',
                permissions: ['get'],
            },
            {
                resources: '/services/category/:categoryId',
                permissions: ['get'],
            },
            {
                resources: '/services/current',
                permissions: ['get'],
            },
            {
                resources: '/services/search',
                permissions: ['post'],
            },
            {
                resources: '/services/provider',
                permissions: ['get'],
            },
            {
                resources: '/uploads/services/picture/:serviceId',
                permissions: ['post'],
            },
            {
                resources: '/uploads/services/picture/:serviceId/delete',
                permissions: ['post'],
            },
        ],
    },
    {
        roles: ['user'],
        allows: [
            {
                resources: '/services',
                permissions: ['get'],
            },
            {
                resources: '/services/service/:serviceId',
                permissions: ['get'],
            },
            {
                resources: '/services/category',
                permissions: ['get'],
            },
            {
                resources: '/services/category/:categoryId',
                permissions: ['get'],
            },
            {
                resources: '/services/current',
                permissions: ['get'],
            },
            {
                resources: '/services/search',
                permissions: ['post'],
            },
            {
                resources: '/services/provider',
                permissions: [],
            },
            {
                resources: '/uploads/services/picture/:serviceId',
                permissions: [],
            },
            {
                resources: '/uploads/services/picture/:serviceId/delete',
                permissions: [],
            },
        ],
    },
    {
        roles: ['guest'],
        allows: [
            {
                resources: '/services',
                permissions: ['get'],
            }, {
                resources: '/services/service/:serviceId',
                permissions: ['get'],
            },
            {
                resources: '/services/category',
                permissions: ['get'],
            },
            {
                resources: '/services/category/:categoryId',
                permissions: ['get'],
            },
            {
                resources: '/services/current',
                permissions: ['get'],
            },
            {
                resources: '/services/search',
                permissions: ['post'],
            },
        ],
    },
]);

/**
 * Check If Services Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = req.user ? req.user.roles : ['guest'];
    // If an service is being processed and the current user created it then allow any manipulation
    if (req.service && req.user && req.service.user && (req.service.user === req.user.id || req.service.user._id === req.user.id)) {
        return next();
    }
    // Check for user roles
    // console.log(req.user.roles, req.route.path, req.method.toLowerCase());
    // console.log(roles);
    servicesAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({ error: 'User Policy Authentication ' });
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({ error: 'User does not have proper rights to this API Route.' });
            }
        }
    });
}
