import Acl from 'acl';

// Using the memory backend
const newsAcl = new Acl(new Acl.memoryBackend());

/**
 * Invoke news Permissions
 */
newsAcl.allow([
    {
        roles: ['admin'],
        allows: [
            {
                resources: '/news',
                permissions: ['*'],
            },
            {
                resources: '/news/:newsId',
                permissions: ['*'],
            },
            {
                resources: '/uploads/news/picture/:newsId',
                permissions: ['*'],
            },
            {
                resources: '/uploads/news/picture/:newsId/delete',
                permissions: ['*'],
            },
        ],
    },
    {
        roles: ['provider', 'user', 'guest'],
        allows: [
            {
                resources: '/news',
                permissions: ['get'],
            },
            {
                resources: '/news/:newsId',
                permissions: ['get'],
            },
            {
                resources: '/uploads/news/picture/:newsId',
                permissions: [],
            },
            {
                resources: '/uploads/news/picture/:newsId/delete',
                permissions: [],
            },
        ],
    },
]);

/**
 * Check If news Policy Allows
 */
export function isAllowed(req, res, next) {
    const roles = req.user ? req.user.roles : ['guest'];
    // If an news is being processed and the current user created it then allow any manipulation
    if (req.news && req.user && req.news.user && (req.news.user.id === req.user.id || req.user.roles.indexOf('admin') > -1)) {
        return next();
    }
    // Check for user roles
    newsAcl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), (err, allowed) => {
        if (err) {
            // An authorization error occurred
            return res.status(401)
                .send({ error: 'User Policy Authentication ' });
        } else {
            if (allowed) {
                // Access granted! Invoke next middleware
                next();
            } else {
                return res.status(403)
                    .send({ error: 'User does not have proper rights to this API Route.' });
            }
        }
    });
}
