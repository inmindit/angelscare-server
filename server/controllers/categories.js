import Category from '../models/category';
import Type from '../models/type';
import Language from '../models/language';
import { translate as translate } from '../controllers/languages';
import _ from 'lodash';
import { getCategoryTypes } from './types';
import async from 'async';


/**
 * Function get type by Id
 */
export function categoryById(req, res, next, id) {
    Category.findById(id, (err, cat) => {
        if (err) {
            return next(err);
        } else if (!cat) {
            return res.status(404)
                .send({ error: 'No Category with that ID has been found' });
        }
        cat = translate(req, cat);
        req.category = cat;
        next();
    });
}

export function getCategories(req, res) {
    const user = req.user;
    Category.aggregate(
        [{
            $lookup:
                {
                    from: 'types',
                    localField: '_id',
                    foreignField: 'category',
                    as: 'types',
                },
        }], (catErr, categories) => {
            if (catErr) {
                return res.status(400)
                    .send({ error: catErr.message });
            }
            const populate = (cat, cb) => {
                Language.populate(cat.translations, { path: 'language' }, () => {
                    Language.populate(cat.types, { path: 'translations.language' }, () => {
                        cat = translate(user, cat);
                        cat.types = translate(user, cat.types);
                        cb();
                    });
                });
            };
            async.each(categories, populate, (err) => {
                if (err) {
                    return res.status(400)
                        .send({ error: err.message });
                }
                res.json(categories);
            });
        });
}

export function getCurrentCategories(req, res) {
    Category.aggregate(
        [{
            $lookup:
                {
                    from: 'services',
                    localField: '_id',
                    foreignField: 'category',
                    as: 'services',
                },
        }, {
            $lookup:
                {
                    from: 'types',
                    localField: '_id',
                    foreignField: 'category',
                    as: 'types',
                },
        }],
        (err, activeCategories) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            activeCategories = _.remove(activeCategories, (category) => {
                console.log(category.services.length);
                return category.services.length === 0;
            });
            activeCategories = translate(req.user, activeCategories);
            activeCategories = activeCategories.map((category) => {
                category.count = category.services.length;
                return category;
                // delete category.services;
            });
            res.json(activeCategories);
        },
    );
}

export function getProviderCategories(req, res) {
    console.log('ACCESS:', req.user.access);
    console.log('ROLES:', req.user.roles);
    if (req.user.roles.indexOf('admin') < -1) {
        Category.find({})
            .exec((err, cats) => {
                if (err) {
                    return res.status(400)
                        .send({ error: err.message });
                }
                console.log(req.user.access, cats);
                cats = translate(req.user, cats);
                res.json(cats);
            });
    } else {
        Category.find({ _id: { $in: req.user.access } })
            .exec((err, cats) => {
                cats = translate(req.user, cats);
                console.log(err, req.user.access, cats);
                res.json(cats);
            });
    }
}

export function saveCategories(req, res) {
    let categories = req.body;
    let newCategories = [];
    _.each(categories, (category) => {
        if (!category.hasOwnProperty('_id')) {
            let newCategory = new Category({
                name: category.name,
                slug: category.slug || category.name || '',
            });
            newCategories.push(newCategory);
        }
    });
    Category.insertMany(newCategories, (errors, created) => {
        if (errors) {
            return res.status(400)
                .send({ error: errors.message });
        }
        getCategories(req, res);
    });
}

export function saveSettings(req, res) {
    let settings = req.body;
    let newCategories = [];
    let newTypes = [];
    let errors = [];
    _.each(settings, (category) => {
        let _category = category;
        if (!category.hasOwnProperty('_id')) {
            let newCategory = new Category({
                name: category.name,
                slug: category.slug || category.name || '',
            });
            //
            newCategory.save((err, saveCategory) => {
                if (err) {
                    return errors.push(err);
                }
                _.each(_category.types, (type) => {
                    if (!type.hasOwnProperty('_id') && category.hasOwnProperty('_id')) {
                        let newType = new Type({
                            name: type.name,
                            slug: type.slug || type.name || '',
                            category: saveCategory._id,
                        });
                        newType.save((err, savedType) => {
                            if (err) {
                                errors.push(err);
                            }
                        });
                    }
                });
            });
        } else {
            _.each(category.types, (type) => {
                if (!type.hasOwnProperty('_id') && category.hasOwnProperty('_id')) {
                    let newType = new Type({
                        name: type.name,
                        slug: type.slug || type.name || '',
                        category: saveCategory._id,
                    });
                    newType.save((err, savedType) => {
                        if (err) {
                            errors.push(err);
                        }
                    });
                }
            });
        }
    });
    console.log(newCategories, newTypes);
    Category.insertMany(newCategories, (errors, createdCategories) => {
        if (errors) {
            return res.status(400)
                .send({ error: errors.message });
        }
        Type.insertMany(newTypes, (errors, createdTypes) => {
            if (errors) {
                return res.status(400)
                    .send({ error: errors.message });
            }
            getCategories(req, res);
        });
    });
}

export function addCategory(req, res) {
    Language.findOne({ tag: 'EN' }, (langErr, lang) => {
        if (langErr) {
            return res.status(400)
                .send({ error: 'There must be created Languages first before creating Categories.' });
        }
        console.log(lang);
        const category = new Category({
            name: req.body.name,
            slug: req.body.slug || req.body.name.toLowerCase() || '',
            translations: [{
                language: lang._id,
                translation: req.body.name,
            }],
        });
        console.log(category);
        category.save((catErr, saved) => {
            if (catErr) {
                return res.status(400)
                    .send({ error: catErr.message });
            }
            getCategories(req, res);
        });
    });
}

export function updateCategory(req, res) {
    Category.findById(req.params.categoryId || req.body._id, (err, category) => {
        if (err) {
            return res.status(400)
                .send({ error: err });
        } else if (!category) {
            return res.status(400)
                .send({ error: 'No category with that ID found' });
        } else {
            category.set({ name: req.body.name || category.name, slug: req.body.slug || category.slug, translations: req.body.translations || [] });
            category.save((saveErr, saved) => {
                if (saveErr) {
                    return res.status(400)
                        .send({ error: saveErr });
                }
                getCategories(req, res);
            });
        }
    });
}

export function updateCategoryTranslations(req, res) {
    Category.update({ _id: req.params.categoryId }, {
        $pull: { translations: { language: req.body.language } },
    }, { multi: true, safe: false }, (removeErr, rowsAffected) => {
        console.log('catRowsAffected1: ', rowsAffected);
        if (removeErr) {
            return res.status(400)
                .send({ error: removeErr });
        } else {
            Category.update({ _id: req.params.categoryId }, {
                $push: { translations: req.body },
            }, (pushErr, rowsAffected2) => {
                console.log('catRowsAffected2: ', rowsAffected2);
                if (pushErr) {
                    return res.status(400)
                        .send({ error: pushErr });
                } else {
                    getCategories(req, res);
                }
            });
        }
    });
}

export function deleteCategory(req, res) {
    const category = req.category;
    if (!!category._id) {
        Type.remove({ category: category._id }, (delTypeErr) => {
            if (delTypeErr) {
                return res.status(400)
                    .send({ error: delTypeErr.message });
            } else {
                category.remove((catErr) => {
                    if (catErr) {
                        return res.status(400)
                            .send({ error: catErr.message });
                    } else {
                        getCategories(req, res);
                    }
                });
            }
        });
    } else {
        getCategories(req, res);
    }
}
