import Language from '../models/language';
import Service from '../models/service';
import mongoose from 'mongoose';
import _ from 'lodash';

/**
 * Get all Languages
 * @param user - Express HTTP Request User Object
 * @param obj - The Object that needs to be translated to user setting language.
 * @returns Object
 */
export function translate(user, obj) {
    if (!!user && !!user.settings && !!user.settings.language) {
        const userLanguage = user.settings.language._id;
        // console.log('Translating. User has user and Language setting', userLanguage, obj);
        if (_.isArray(obj)) {
            // console.log('Translating array of Objects.');
            _.forEach(obj, (sub) => {
                // console.log('Translating SubObject of Array: ', sub.translations);
                const translation = _.find(sub.translations, { language: userLanguage }) || _.find(sub.translations, { language: { _id: userLanguage } }) || null;
                // console.log('Translating SubCategory: ', translation);
                if (translation) {
                    sub.name = translation.translation;
                }
                // delete sub.translations;
            });
        } else {
            // console.log('Translating:', obj.translations);
            const translation = _.find(obj.translations, { language: userLanguage }) || _.find(obj.translations, { language: { _id: userLanguage } }) || null;
            // console.log('Translating Object: ', translation);
            if (translation) {
                obj.name = translation.translation;
            }
            // delete obj.translations;
        }
    } else {
        console.log('FAILED TRANSLATION. USER OR LANGUAGE SETTING MISSING.');
    }
    return obj;
}

/**
 * Get all Languages
 * @param user - Express HTTP Request User Object
 * @param service - The Object that needs to be translated to user setting language.
 * @returns Object
 */
export function translateService(user, service) {
    if (!!user && !!user.settings && !!user.settings.language) {
        const userLanguage = user.settings.language._id;
        // console.log('Translating. User has user and Language setting', userLanguage, obj);
        Service.findOne({})
        .exec((serviceErr, translatedService) => {
            if (serviceErr || !translatedService) {
                return service;
            } else {
                return translatedService;
            }
        });
    } else {
        console.log('FAILED TRANSLATION. USER OR LANGUAGE SETTING MISSING.');
    }
    return service;
}

/**
 * Get all Languages
 * @param req
 * @param res
 * @returns void
 */
export function getAllLanguages(req, res) {
    Language.find()
        .sort('-dateAdded')
        .exec((err, languages) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            res.json(languages);
        });
}

/**
 * Create a Language Entry
 * @param req
 * @param res
 * @returns void
 */
export function addLanguage(req, res) {
    if (!req.body.tag || !req.body.name) {
        return res.status(403)
            .send({ error: 'Language must have name and tag.' });
    }

    const newLanguage = new Language({
        name: req.body.name,
        tag: req.body.tag,
    });

    newLanguage.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        }
        res.json(saved);
    });
}

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function updateLanguage(req, res) {
    const language = _.extend(req.language, req.body);
    language.updated = Date.now();
    language.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json(saved);
        }
    });
}

/**
 * Get a single language
 * @param req
 * @param res
 * @returns void
 */
export function getLanguage(req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.languageId)) {
        Language.findOne({ _id: req.params.languageId })
            .exec((err, language) => {
                if (err) {
                    res.status(400)
                        .send({ error: err.message });
                }
                res.json(language);
            });
    } else {
        Language.findOne({ tag: req.params.languageId })
            .exec((err, language) => {
                if (err) {
                    res.status(400)
                        .send({ error: err.message });
                }
                res.json(language);
            });
    }
}

/**
 * Delete a language
 * @param req
 * @param res
 * @returns void
 */
export function deleteLanguage(req, res) {
    Language.findOne({ _id: req.params.languageId }, (err, language) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        }

        language.remove(() => {
            res.status(200)
                .end();
        });
    });
}

/**
 * Function to populate language if languageId is available
 * @param req
 * @param res
 * @param next
 * @param id
 * @returns void
 */
export function languageByID(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400)
            .send({ error: 'LanguageID is invalid' });
    }
    Language.findById(id)
        .populate('user', 'displayName')
        .exec((err, language) => {
            if (err) {
                return next(err);
            } else if (!language) {
                return res.status(404)
                    .send({ error: 'No Language with that identifier has been found' });
            }
            req.language = language;
            next();
        });
}
