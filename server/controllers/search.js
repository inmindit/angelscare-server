import Category from '../models/category';
import mongoose from 'mongoose';
import Type from '../models/type';
import Language from '../models/language';
import { translate as translate } from '../controllers/languages';
import _ from 'lodash';
import async from 'async';


export function search(req, res) {
    const user = req.user;
    let aggregate = [];
    let moduleData = (isType) => {
        return [
            {
                $lookup:
                    {
                        from: 'services',
                        localField: isType ? 'category' : '_id',
                        foreignField: 'category',
                        as: 'services',
                    },
            },
            {
                $lookup:
                    {
                        from: 'news',
                        localField: isType ? 'category' : '_id',
                        foreignField: 'category',
                        as: 'news',
                    },
            },
            {
                $lookup:
                    {
                        from: 'clubs',
                        localField: isType ? 'category' : '_id',
                        foreignField: 'category',
                        as: 'clubs',
                    },
            },
        ];
    };
    if (!!req.body.category && mongoose.Types.ObjectId.isValid(req.body.category)) {
        aggregate.push(
            {
                $match: {
                    _id: mongoose.Types.ObjectId(req.body.category),
                },
            },
            {
                $lookup:
                    {
                        from: 'types',
                        localField: '_id',
                        foreignField: 'category',
                        as: 'types',
                    },
            });
        Array.prototype.push.apply(aggregate,moduleData(false));
        Category.aggregate(aggregate, (catErr, categories) => {
            if (catErr) {
                return res.status(400)
                    .send({ error: catErr.message });
            }
            console.log('CAT: ', aggregate, categories);
            // res.json(categories);
            const populate = (cat, cb) => {
                Language.populate(cat.translations, { path: 'language' }, () => {
                    Language.populate(cat.types, { path: 'translations.language' }, () => {
                        cat = translate(user, cat);
                        cat.types = translate(user, cat.types);
                        cb();
                    });
                });
            };
            async.each(categories, populate, (err) => {
                if (err) {
                    return res.status(400)
                        .send({ error: err.message });
                }
                res.json(categories[0]);
            });
        });
    } else if (!!req.body.type && mongoose.Types.ObjectId.isValid(req.body.type)) {
        aggregate.push({ $match: { _id: mongoose.Types.ObjectId(req.body.type) } });
        Array.prototype.push.apply(aggregate, moduleData(true));
        Type.aggregate(aggregate, (typeErr, types) => {
            if (typeErr) {
                return res.status(400)
                    .send({ error: typeErr.message });
            }
            // res.json(types);
            console.log('TYPE:', aggregate);
            const populate = (type, cb) => {
                Type.populate(type, { path: 'category' }, () => {
                    Language.populate(type.translations, { path: 'language' }, () => {
                        Language.populate(type.category, { path: 'translations.language' }, () => {
                            type = translate(user, type);
                            type.category = translate(user, type.category);
                            cb();
                        });
                    });
                });
            };
            async.each(types, populate, (err) => {
                if (err) {
                    return res.status(400)
                        .send({ error: err.message });
                }
                res.json(types[0]);
            });
        });
    } else {
        res.json({});
    }
}
