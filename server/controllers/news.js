import News from '../models/news';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';
import mongoose from 'mongoose';
import Config from '../config';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import _ from 'lodash';

/**
 * Get all posts
 * @param req
 * @param res
 * @returns void
 */
export function getAllNews(req, res) {
    News.find()
        .sort('-dateAdded')
        .exec((err, posts) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            res.json(posts);
        });
}

/**
 * List of News by Category
 */
export function getNewsByCategory(req, res) {
    News.find({ category: req.category._id })
        .sort('-created')
        .exec((err, news) => {
            if (err) {
                console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(news);
            }
        });
}
/**
 * List of News by Type
 */
export function getNewsByType(req, res) {
    News.find({ type: req.type._id })
        .sort('-created')
        .exec((err, news) => {
            if (err) {
                console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(news);
            }
        });
}


/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function addNews(req, res) {
    if (!req.body.title || !req.body.content) {
        return res.status(403)
            .send({ error: 'News article must have Title and Content.' });
    }

    delete req.body.image;
    if (req.body.youtube.length > 0) {
        delete req.body.image;
    }
    const newNews = new News(req.body);
    newNews.user = req.user;
    // Let's sanitize inputs
    newNews.title = sanitizeHtml(newNews.title);
    newNews.name = sanitizeHtml(newNews.name);
    newNews.content = sanitizeHtml(newNews.content);

    newNews.slug = slug(newNews.title.toLowerCase(), { lowercase: true });
    newNews.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send(err);
        }
        res.json(saved);
    });
}

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function updateNews(req, res) {
    delete req.body.image;
    const news = _.extend(req.news, req.body);
    news.updated = Date.now();
    news.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json(saved);
        }
    });
}

/**
 * Get a single news
 * @param req
 * @param res
 * @returns void
 */
export function getNews(req, res) {
    News.findOne({ _id: req.params.newsId })
        .exec((err, news) => {
            if (err) {
                res.status(400)
                    .send({ error: err.message });
            }
            res.json(news);
        });
}

/**
 * Delete a news
 * @param req
 * @param res
 * @returns void
 */
export function deleteNews(req, res) {
    News.findOne({ _id: req.params.newsId }, (err, news) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        }

        news.remove(() => {
            res.status(200)
                .end();
        });
    });
}

/**
 Helper functions
 */
const deleteNewsPictures = (filePath) => {
    try {
        console.log(filePath);
        filePath = filePath.substr(filePath.lastIndexOf('/') + 1, filePath.length);
        console.log(filePath);
        fs.unlinkSync(Config.uploads.serviceUpload.dest + filePath, (err) => {
            if (err) {
                console.log(err);
                return {
                    error: true,
                    message: err,
                };
            } else {
                return {
                    error: false,
                    message: `Successfully deleted : ${filePath}`,
                };
            }
        });
    } catch (e) {
        return e;
    }
};

/**
 * Change profile picture
 */
export function removeNewsPicture(req, res) {
    let imgPath = req.body.filename;
    deleteNewsPictures(imgPath);
    const news = req.news;
    /* let images = [].concat(news.images);
    images = _.remove(images,(img) => {
        return _.indexOf(img,imgName) > -1;
    }); */
    delete news.image;
    news.save((saveError, savedService) => {
        if (saveError) {
            return res.status(400)
                .send({ error: saveError });
        } else {
            console.log('Removed picture from EVENT');
            res.json(savedService.image);
        }
    });
}

export function addNewsPicture(req, res) {
    const news = req.news;
    // let message = null;
    const storage = multer.diskStorage({
        destination: (destReq, file, cb) => {
            cb(null, Config.uploads.newsUpload.dest);
        },
        filename: (fileReq, file, cb) => {
            cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
        },
        limit: Config.uploads.newsUpload.limits,
    });
    const upload = new multer({ storage }).single('news');
    // Filtering to upload only images
    upload(req, res, (uploadError) => {
        if (!req.file) {
            // console.log('FILE ERROR');
            return res.status(400)
                .send({
                    message: 'No file to received.',
                });
        } else if (uploadError) {
            // console.log('UPLOAD ERROR');
            return res.status(400)
                .send({
                    message: 'Error occurred while uploading profile picture',
                    error: uploadError,
                });
        } else {
            const file = '/api' + req.file.destination.substr(1, req.file.destination.length) + req.file.filename;
            if (news) {
                news.image = file;
                news.save((saveError, savedService) => {
                    if (saveError) {
                        // console.log('NEWS SAVE IMAGE ERROR');
                        return res.status(400)
                            .send({ error: saveError });
                    } else {
                        // console.log('Image saved.');
                        return res.json(savedService.image);
                    }
                });
            } else {
                // console.log('Save picture to NEW news');
                res.status(200)
                    .send(file);
            }
        }
    });
}

/**
 * Function to populate news if newsId is available
 * @param req
 * @param res
 * @param next
 * @param id
 * @returns void
 */
export function newsByID(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400)
            .send({ error: 'NewsID is invalid' });
    }
    News.findById(id)
        .populate('user', 'displayName')
        .exec((err, news) => {
            if (err) {
                return next(err);
            } else if (!news) {
                return res.status(404)
                    .send({ error: 'No News with that identifier has been found' });
            }
            req.news = news;
            next();
        });
}
