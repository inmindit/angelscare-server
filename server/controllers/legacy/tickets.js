/* jshint loopfunc: true */

const _ = require('lodash');
import Seat from '../models/seat';
import Service from '../models/service';
import Category from '../models/ticket_category';
import Hall from '../models/ticket_hall';
import Order from '../models/order';

function error(res, err, message) {
    if (err) console.log('\nERROR : ' + err + '\n\n');
    if (message) console.log('Message : ' + message.message + '\n\n');
    if (err) {
        return res.status(400)
            .send({ error: err.message });
    } else {
        return res.status(400)
            .send({ error: 'Unknown error occured. It has been recorded and will be dealt with.' });
    }
}

export function changedTicketsType(req, res) {
    Services.findById(req.params.serviceId)
        .exec((err, event) => {
            if (req.body.ticketsType !== event.ticketsType) {
                Order.find({ event: req.params.eventId })
                    .exec((err2, order) => {
                        if (!err2 && order.length >= 1) {
                            return res.status(400)
                                .json({
                                    message: 'Не може да правите промени ! Вече има продадени билети !',
                                    error: err2.message,
                                });
                        } else {
                            event.ticketsType = (event.ticketsType === 'halls') ? 'categories' : 'halls';
                            event.save((err3, newService) => {
                                if (!err3 && newService) {
                                    Seat.find({ event: event._id })
                                        .remove()
                                        .exec((err4, newSeats) => {
                                            // console.log(err,seats);
                                        });
                                    return res.json(event);
                                } else {
                                    return error(res, err);
                                }
                            });
                        }
                    });
            } else {
                return res.status(200)
                    .message('Nothing to change');
            }
        });
}

export function categoriesGet(req, res) {
    Category.find({ event: req.params.eventId })
        .exec((err, data) => {
            if (err) {
                error(res, err);
            } else {
                res.json(data);
            }
        });
}

export function categoriesPost(req, res) {
    const cat = new Category({
        description: req.body.description,
        event: req.body.event,
        name: req.body.name,
        order: req.body.order,
        price: req.body.price,
    });
    if (req.body.event.ticketsType === 'categories') {
        cat.seats = 0;
    }
    cat.save((err, data) => {
        if (err) {
            error(res, err);
        } else if (!data) {
            return res.status(400)
                .send({ error: 'No Tickets with that identifier has been found' });
        } else {
            Category.find({ event: req.params.eventId })
                .exec((e, d) => {
                    res.json({ categories: d });
                });
        }
    });
}

export function categoriesPut(req, res) {
    Category.findById(req.params.catId)
        .exec((err, data) => {
            if (err || !data) {
                error(res, err);
            } else if (!err && data) {
                // s
                data.name = req.body.name;
                data.description = req.body.description;
                data.price = req.body.price;
                data.order = req.body.order;
                data.color = req.body.color;
                // data.seats = req.body.seats;
                //
                data.save((e, cat) => {
                    if (e) {
                        res.status(400)
                            .send({ error: e.message });
                    } else {
                        res.json(cat);
                    }
                });
            }
        });
}

export function categoriesDelete(req, res) {
    Category.findByIdAndRemove(req.params.catId, (err, data) => {
        if (err) {
            res.status(400)
                .send({ error: err.message });
        } else {
            // Check for category seats !
            Seat.find({ category: req.params.catId })
                .remove()
                .exec((err2, seats) => {
                    if (err2) {
                        res.status(400)
                            .send({ error: err2.message });
                    }
                    console.log(seats);
                });
            //
            Category.find({ event: req.params.eventId })
                .exec((err3, d) => {
                    if (err3) {
                        return res.status(400)
                            .send({ error: err3.message });
                    } else {
                        res.json({ categories: d });
                    }
                });
        }
    });
}

export function hallsGet(req, res) {
    Hall.find({ event: req.params.eventId })
        .exec((err, data) => {
            if (err) {
                error(res, err);
            } else {
                res.json(data);
            }
        });
}

export function hallsPost(req, res) {
    let order = 0;
    Hall.findOne()
        .sort('-order')
        .exec((err, hall) => {
            if (!err && hall) {
                order = hall.order + 1;
            }
        });
    const hall = new Hall({
        description: req.body.description,
        event: req.body.event,
        name: req.body.name,
        order: req.body.order || order,
    });
    hall.save((err, data) => {
        if (err) {
            error(res, err);
        } else {
            // Init 3x3 seats schema for the new hall
            // grab a cat fast
            Category.find({ event: req.params.eventId })
                .sort('-order')
                .limit(1)
                .exec((err2, cat) => {
                    //
                    cat = cat[0];
                    // console.log(cat);
                    for (let i = 1; i <= 3; i++) {
                        for (let y = 1; y <= 3; y++) {
                            Seat.create({
                                event: req.body.event,
                                hall: data._id,
                                category: cat._id,
                                row: i,
                                seat: y,
                                number: y,
                                divided: true,
                                state: 'free',
                            }, (e, seat) => {
                                if (e || !seat) {
                                    error(res, e);
                                } else {
                                }
                            });
                        }
                    }    //
                });
            Hall.find({ event: req.params.eventId })
                .exec((e, d) => {
                    res.json({ halls: d });
                });
        }
    });
}

export function hallsPut(req, res) {
    Hall.findById(req.params.hallId)
        .exec((err, data) => {
            if (err || !data) {
                error(res, err);
            } else {
                //
                data.name = req.body.name;
                data.description = req.body.description;
                data.order = req.body.order;
                //
                data.save((e, hall) => {
                    if (e) {
                        return res.status(400)
                            .send({ error: e });
                    } else {
                        hall.editMode = false;
                        res.json(hall);
                    }
                });
            }
        });
}

export function hallsDelete(req, res) {
    Hall.findByIdAndRemove(req.params.hallId, (err, data) => {
        if (err) {
            error(res, err);
        } else {
            // delete seats for the hall
            Seat.find({ hall: req.params.hallId })
                .remove()
                .exec((err2, rows) => {
                    if (err2) {
                        error(res, err2);
                    }
                    // console.log(rows);
                });
            //
            Hall.find({ event: req.params.eventId })
                .exec((err3, d) => {
                    if (err3) {
                        error(res, err3);
                    } else {
                        res.json({ halls: d });
                    }
                });
        }
    });
}

export function seatsQuery(req, res) {
    // const query = req.body.query;
    Seat.find(req.body.find)
        .populate(req.body.populate || null)
        .sort(req.body.sort || null)
        .limit(req.body.limit || null)
        .exec((err, seats) => {
            if (!err && seats) {
                console.log(seats);
                res.json(seats);
            } else {
                error(res, err);
            }
        });
}

export function seatsGet(req, res) {
    Seat.find({ event: req.params.eventId })
        .exec((err, data) => {
            if (err) {
                console.log('Cannot find eventId in seatsGet method');
                error(res, err);
            } else {
                console.log('STATUS: 200');
                // console.log(res.json(data));
                res.json(data);
            }
        });
}

export function seatsPost(req, res) {
    const action = req.body.action || false;
    // console.log('ACTION: ', action, req.body);
    if (action) {
        // console.log('ACTION2: ', action);
        if (action === 'row-seat') {
            const num = req.body.seat.seat + 1;
            const newSeat = {
                event: req.body.seat.event,
                row: req.body.seat.row,
                seat: num,
                number: num,
                hall: req.body.seat.hall,
                category: req.body.seat.category._id,
                state: 'free',
            };
            // console.log(num,newSeat);
            Seat.create(newSeat, (err, seat) => {
                if (!err && seat) {
                    // console.log(seat);
                    res.json(seat);
                } else {
                    console.log(err);
                    error(res, err);
                }
            });
        } else if (action === 'category') {
            console.log('CATEGORY ACTION', req.body);
            if (req.body.seats < 0) {
                res.status(400)
                    .send({ error: 'Не може да има отрицателен брой места !' });
            } else if (req.body.seats === 0) {
                Seat.find({})
                    .and([{ event: req.body.category.event }, { category: req.body.category._id }])
                    .remove((err, seats) => {
                        if (err) {
                            // console.log(delErr);
                            Seat.find({})
                                .and([{ event: req.body.category.event }, { category: req.body.category._id }])
                                .count((err2, result) => {
                                    Category.findOne({ _id: req.body.category._id })
                                        .update({ seats: result });
                                    console.log('Updating Category Seats Count to : ', result);
                                });
                            error(res, err);
                        } else {
                            res.status(200).send([{ error: 'Успешно занулихте местата в категорията!' }]);
                        }
                    });
            } else if (req.body.seats > 0) {
                Seat.find({})
                    .and([{ event: req.body.category.event }, { category: req.body.category._id }])
                    .sort({ number: -1 })
                    .exec((err, seats) => {
                        if (!err && seats.length >= 1) {
                            console.log('CURRENT SEATS FOUND : ', seats);
                            const seat = seats[0];
                            const catSeat = {
                                event: seat.event,
                                number: seats.length,
                                category: seat.category,
                                state: 'free',
                            };
                            console.log(seat, req.body);
                            if (req.body.seats > seats.length) {
                                console.log('REQUESTED MORE SEATS THAN BEFORE');
                                for (let i = seats.length + 1; i <= req.body.seats; i++) {
                                    catSeat.number = i;
                                    //
                                    Seat.create(catSeat, (er, s) => {
                                        if (!er && s) {

                                        } else {
                                            console.log('Seat Create Error', er, s);
                                            error(res, err);
                                        }
                                    });
                                }
                                Seat.find({})
                                    .and([{ event: req.body.category.event }, { category: req.body.category._id }])
                                    .exec((err2, foundSeats) => {
                                        if (!err2 && foundSeats) {
                                            Category.findById(req.body.category._id)
                                                .exec((catErr, cat) => {
                                                    cat.seats = foundSeats.length;
                                                    cat.save();
                                                });
                                            res.json({ foundSeats });
                                        }
                                    });
                            } else if (req.body.seats < seats.length) {
                                console.log('REQUESTED LESS SEATS THAN BEFORE');
                                console.log('Deleting old seats');
                                Seat.find({})
                                    .and([{ event: req.body.category.event }, { category: req.body.category._id }, { number: { $gt: req.body.seats } }])
                                    .remove((delErr, seats) => {
                                        console.log('Deleted old seats');
                                        if (delErr) {
                                            // console.log(delErr);
                                            error(res, delErr);
                                        } else {
                                            Seat.find({})
                                                .and([{ event: req.body.category.event }, { category: req.body.category._id }])
                                                .exec((err2, foundSeats) => {
                                                    if (!err2 && foundSeats) {
                                                        Category.findById(req.body.category._id)
                                                            .exec((catErr, cat) => {
                                                                cat.seats = foundSeats.length;
                                                                cat.save();
                                                            });
                                                        res.json({ foundSeats });
                                                    }
                                                });
                                        }
                                        /* if(!delErr){
                                         console.log('Creating new seats');
                                         for (let i = 1; i <= req.body.seats; i++) {
                                         catSeat.number = i;
                                         //
                                         Seat.create(catSeat, function (er, s) {
                                         if (er) {
                                         if(i===req.body.seats) error(res, er);
                                         }
                                         });
                                         }
                                         console.log('Created new Seats');
                                         //
                                         Seat.find({event: req.params.eventId}).populate([
                                         'category',
                                         'ticket'
                                         ]).exec(function (errr, seats) {
                                         if (!errr && seats) {
                                         Category.findById(req.body.category._id).exec(function(catErr,cat){
                                         cat.seats = seats.length;
                                         cat.save();
                                         });
                                         res.json({seats: seats});
                                         }
                                         });
                                         }else{
                                         error(res, delErr);
                                         }*/
                                    });
                            } else {
                                error(res, null, { error: 'Не е направена промяна в броя на местата!' });
                            }
                        } else if (!err && seats.length <= 0) {
                            console.log('NO CURRENT SEATS TO DELETE. PROCEED TO CREATING NEW SEATS');
                            const catSeat = {
                                event: req.params.eventId,
                                number: 1,
                                category: req.body.category,
                                state: 'free',
                            };
                            const newSeats = [];
                            for (let i = 1; i <= req.body.seats; i++) {
                                catSeat.number = i;
                                newSeats.push(catSeat);
                            }
                            Seat.create(newSeats, (er, dbSeats) => {
                                if (!er || dbSeats) {
                                    Seat.find({ event: req.params.eventId })
                                        .exec((errr, foundSeats) => {
                                            if (!errr && foundSeats) {
                                                Category.findById(req.body.category._id)
                                                    .exec((catErr, cat) => {
                                                        if (!cat || err) {

                                                        } else {
                                                            cat.seats = foundSeats.length;
                                                            cat.save();
                                                        }
                                                    });
                                                res.json({ foundSeats });
                                            }
                                        });
                                } else {
                                    console.log(er);
                                    if (i === req.body.seats) error(res, er);
                                }
                            });
                            //
                        } else {
                            error(res, err);
                        }
                    });
            } else {
                res.status(400)
                    .send({ error: 'Грешка в заявката' });
            }
        }
        else {
            Seat.find({
                event: req.params.eventId,
                hall: req.body.hall,
            })
                .sort({
                    row: -1,
                    seat: -1,
                })
                .limit(1)
                .exec((err, seats) => {
                    const seat = seats[0];
                    if (!err && seat) {
                        if (action === 'column') {
                            const newSeat = {
                                event: seat.event,
                                row: seat.row,
                                seat: req.body.seat,
                                number: req.body.seat,
                                hall: seat.hall,
                                category: seat.category,
                                state: 'free',
                            };
                            for (let i = 1; i <= seat.row; i++) {
                                newSeat.row = i;
                                //
                                Seat.create(newSeat, (er, s) => {
                                    if (!er && s) {
                                    } else {
                                        error(res, err);
                                    }
                                });
                            }
                        } else if (action === 'row') {
                            const newSeat = {
                                event: seat.event,
                                row: parseInt(seat.row) + 1,
                                seat: seat.seat,
                                number: seat.seat,
                                hall: seat.hall,
                                category: seat.category,
                                state: 'free',
                            };
                            // console.log('\nNEW MAX SEAT : ',newSeat);
                            for (let i = 1; i <= req.body.width; i++) {
                                newSeat.seat = i;
                                newSeat.number = i;
                                //
                                Seat.create(newSeat, (er, s) => {
                                    if (!er && s) {
                                    } else {
                                        error(res, er);
                                    }
                                });
                            }
                        }
                        Seat.find({ event: req.params.eventId })
                            .exec((errr, foundSeats) => {
                                if (!errr && foundSeats) {
                                    res.json({ foundSeats });
                                }
                            });
                    } else {
                        error(res, err);
                    }
                });
        }
    } else {
        res.status(400)
            .send({ error: 'Action not allowed !' });
    }
}

export function seatsPut(req, res) {
    Seat.findById(req.body._id)
        .exec((err, data) => {
            if (err) {
                error(res, err);
            } else {
                // Update seat status
                data.category = req.body.category._id ? req.body.category._id : req.body.seat.category;
                data.number = req.body.number;
                data.state = req.body.state;
                //
                data.save((e, s) => {
                    if (e) {
                        res.status(400)
                            .send({ error: e });
                    } else {
                        res.json(s);
                    }
                });
            }
        });
}

export function seatsDelete(req, res) {
    console.log(req.params, req.body);
    // res.status(200);
    Seat.findOne({ event: req.params.eventId, _id: req.params.seatId })
        .exec((err, seat) => {
            if (err) {
                error(res, err);
            } else if (seat) {
                console.log('REQUEST SEAT REMOVE :', req.params.seatId, seat);
                seat.remove((err2) => {
                    if (err2) {
                        error(res, err2, 'SEATS DELETE ERR');
                    } else {
                        res.json({ seat: {} });
                    }
                });
            } else {

            }
        });
}

export function orderUserHistoryGet(req, res) {
    res.json({ orders: [] });
}
