/**
 * Module dependencies.
 */
import mongoose from 'mongoose';
import User from '../models/user';
import Order from '../models/order';
import Contact from '../models/contact';

let PasswordGenerator = require('strict-password-generator').default;
import { invitationEmail } from './contact';


PasswordGenerator = new PasswordGenerator();

/**
 * Show the current user
 */
export function showUser(req, res) {
    res.json(req.model);
}

/**
 * Update a User
 */
export function inviteUser(req, res) {
    // Init Variables
    const generatedPassword = PasswordGenerator.generatePassword({
        exactLength: 16,
        number: true,
        specialCharacter: true,
    });
    const user = new User({
        email: req.body.email,
        username: req.body.username,
        password: generatedPassword,
        firstName: req.body.firstName || '',
        lastName: req.body.lastName || '',
        access: req.body.access || [],
        status: 'invited',
        roles: [req.body.role] || ['provider'],
        provider: 'local',
    });
    // console.log('Generated Password: ', user.password);
    // Then save the user
    user.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            invitationEmail({
                username: saved.username,
                password: generatedPassword,
                name: saved.displayName,
                email: saved.email,
            }, req, res, (err2) => {
                if (!!err2) {
                    res.status(400)
                        .send({ error: err2.message });
                } else {
                    res.status(200)
                        .send({ message: 'User successfully invited. Email sent to user for further instructions.' });
                }
            });
            //
        }
    });
}

export function updateUserRolesAndAccess(req, res) {
    const user = req.model;
    // For security purposes only merge these parameters
    if (req.body.hasOwnProperty('roles')) {
        user.roles = req.body.roles;
    }
    if (req.body.hasOwnProperty('access')) {
        user.access = req.body.access;
    }
    user.save((err) => {
        if (err) {
            res.status(400)
                .send({ error: err.message });
        }
        res.json(user);
    });
}

/**
 * Delete a user
 */
export function deleteUser(req, res) {
    const user = req.model;
    user.remove((err) => {
        if (err) {
            res.status(400)
                .send({ error: err.message });
        }
        res.json(user);
    });
}

/**
 * List of Users
 */
export function listUsers(req, res) {
    // console.log("API - GET - /admin/users");
    User.find({}, '-salt -password')
        .sort('-created')
        .exec((err, users) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            // console.log(users);
            res.json(users);
        });
}

/**
 * User middleware
 */
export function userByID(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400)
            .send({ error: 'User is invalid' });
    }
    User.findById(id, '-salt -password')
        .exec((err, user) => {
            if (err) {
                next(err);
            } else if (!user) {
                console.log(`Failed to load user ${id}`);
                return res.status(404)
                    .send({ error: 'User not found. ID:'+id });
            }
            req.model = user;
            next();
        });
}

export function getOrders(req, res) {
    Order.find({})
        .sort('-created')
        .exec((err, orders) => {
            if (err || !orders) {
                res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(orders);
            }
        });
}

export function getContactRequests(req, res) {
    Contact.find({})
        .sort('-created')
        .populate('user provider service club')
        .exec((err, orders) => {
            if (err || !orders) {
                res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(orders);
            }
        });
}

export function model(req, res) {
    return res;
}

export function returnUser(req, res) {
    return res.json(req.user);
}
