import Order from '../models/order';
import Service from '../models/service';
import Payment from '../models/payment';

let PasswordGenerator = require('strict-password-generator').default;
import { guestOrderInvitationEmail } from './contact';
import User from '../models/user';


export function orderByID(req, res, next, id) {
    Order.findById({ id }, (err, order) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else if (order) {
            res.json(order);
        } else {
            res.status(400)
                .send({ error: 'Грешна заявка !' });
        }
    });
}

export function createGuestOrder(req, res) {
    // console.log("ORDER GET req : ",{event: req.params.eventId, user: req.user._id},req.user);
    if (!!req.user) {
        Order.create({
            user: req.user._id,
            service: req.body.service,
            quantity: req.body.quantity,
            status: 'in-progress',
        }, (err, newOrder) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json({
                    newOrder,
                });
            }
        });
    } else {
        const generatedPassword = PasswordGenerator.generatePassword({
            exactLength: 16,
            number: true,
            specialCharacter: true,
        });
        const user = new User({
            email: req.body.email,
            username: req.body.email,
            password: generatedPassword,
            firstName: req.body.firstName || '',
            lastName: req.body.lastName || '',
            status: 'invited',
            roles: ['user'],
            provider: 'local',
        });
        // Then save the user
        user.save((err, saved) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            } else {
                guestOrderInvitationEmail({
                    username: saved.username,
                    password: generatedPassword,
                    name: saved.displayName,
                    email: saved.email,
                }, req, res, (err2) => {
                    if (!!err2) {
                        res.status(400)
                            .send({ error: err2.message });
                    } else {
                        Order.create({
                            user: saved._id,
                            service: req.body.service,
                            quantity: req.body.quantity,
                            status: 'in-progress',
                        }, (orderErr, newOrder) => {
                            if (orderErr) {
                                return res.status(400)
                                    .send({ error: err.message });
                            } else {
                                res.json(newOrder);
                            }
                        });
                    }
                });
            }
        });
    }
}

export function createOrder(req, res) {
    // console.log("ORDER GET req : ",{event: req.params.eventId, user: req.user._id},req.user);
    Order.create({
        user: req.user._id,
        service: req.body.service,
        quantity: req.body.quantity || 1,
        status: 'in-progress',
    }, (err, newOrder) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json({
                newOrder,
            });
        }
    });
}

export function orderVoid(req, res) {
    Order.findById(req.params.orderId)
        .exec((err, order) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            } else if (order) {
                order.state = 'void';
                order.save((err2, newOrder) => {
                    if (err2) {
                        return res.status(400)
                            .send({ error: err.message });
                    } else {
                        res.status(200)
                            .send({ error: 'Order has been successfully Voided.' });
                    }
                });
            } else {
                return res.status(400)
                    .send({ error: 'Bad Request' });
            }
        });
}

export function orderHistory(req, res) {
    Order.find({
        user: req.user._id,
    })
        .sort('-created')
        .exec((err, orders) => {
            if (err || !orders) {
                res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(orders);
            }
        });
}

export function orderProviderHistory(req, res) {
    Service.find({ user: req.user._id })
        .exec((serviceErr, services) => {
            if (serviceErr || !services) {
                return res.status(400)
                    .send({ error: serviceErr.message });
            } else {
                const serviceIds = services.map((service) => {
                    return service._id;
                });
                console.log(serviceIds);
                Order.find({ service: { $in: serviceIds } })
                    .exec((err, orders) => {
                        if (err || !orders) {
                            return res.status(400)
                                .send({ error: err.message });
                        } else {
                            res.json(orders);
                        }
                    });
            }
        });
}

export function currentOrder(req, res) {
    Order.findOne({
        _id: req.params.orderId,
        user: req.user._id,
        state: 'in-progress',
    })
        .exec((err, order) => {
            if (err) {
                res.status(400)
                    .send({ error: err.message });
            } else if (!order) {
                res.json([]);
            } else {
                res.json(order);
            }
        });
}

export function startPayment(req, res) {
    const payment = new Payment(req.body);
    payment.save((err, saved) => {
        if (err || !payment) {
            res.status(400)
                .send({ error: err.message });
        } else {
            res.json(saved);
        }
    });
}

export function paymentHistory(req, res) {
    Payment.find({ user: req.user._id });
}
