import mongoose from 'mongoose';
import Type from '../models/type';
import Language from '../models/language';
import _ from 'lodash';
import { getCategories } from './categories';
import { translate } from './languages';

/**
 * Function get type by Id
 */
export function typeById(req, res, next, id) {
    // console.log("typeId:",id);
    Type.findById(id, (err, t) => {
        if (err) {
            return next(err);
        } else if (!t) {
            return res.status(404)
                .send({ error: 'No Type with that ID has been found' });
        } else {
            t = translate(req.user, t);
            req.type = t;
            next();
        }
    });
}

export function getTypes(req, res) {
    Type.find({}, (err, types) => {
            if (err) {
                return res.status(400)
                    .send(err);
            }
            types = translate(req.user, types);
            res.json(types);
        },
    );
}

export function getCategoryTypes(req, res) {
    Type.find({ category: req.params.categoryId }, (err, types) => {
            if (err) {
                return res.status(400)
                    .send(err);
            }
            types = translate(req.user, types);
            res.json(types);
        },
    );
}

export function getCurrentTypes(req, res) {
    Type.aggregate(
        {
            $lookup:
                {
                    from: 'services',
                    localField: '_id',
                    foreignField: 'type',
                    as: 'services',
                },
        }, (err, activeTypes) => {
            if (err) {
                return res.status(400)
                    .send(err);
            }
            _.each(activeTypes, (type) => {
                type.count = type.services.length;
                // delete type.events;
            });
            _.remove(activeTypes, (type) => {
                return type.count < 1;
            });
            activeTypes = translate(req.user, activeTypes);
            res.json(activeTypes);
        },
    );
}

export function saveTypes(req, res) {
    Language.findOne({ tag: 'EN' }, (langErr, lang) => {
        if (langErr) {
            return res.status(400)
                .send({ error: 'There must be created Languages first before creating Categories.' });
        }
        let types = req.body;
        let newTypes = [];
        _.each(types, (type) => {
            if (!type.hasOwnProperty('_id')) {
                let newType = new Type({
                    name: type.name,
                    slug: type.slug || type.name || '',
                    category: type.category,
                    translations: [{
                        language: lang._id,
                        translation: type.name,
                    }],
                });
                newTypes.push(newType);
            }
        });
        Type.insertMany(newTypes, (errors, created) => {
            if (errors) {
                return res.status(400)
                    .send({ error: errors.message });
            }
            getCategories(req, res);
        });
    });
}

export function addType(req, res) {
    Language.findOne({ tag: 'EN' }, (langErr, lang) => {
        if (langErr) {
            return res.status(400)
                .send({ error: 'There must be created Languages first before creating Categories.' });
        }
        console.log(lang);
        const type = new Type({
            name: req.body.name,
            slug: req.body.slug || req.body.name || '',
            category: req.body.category,
            translations: [{
                language: lang._id,
                translation: req.body.name,
            }],
        });
        type.save((err, saved) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            console.log(type,  saved);
            getCategories(req, res);
        });
    });
}

export function updateType(req, res) {
    Type.findById(req.body._id, (err, type) => {
        type.set({ name: req.body.name, slug: req.body.slug });
        type.save((saveErr, saved) => {
            if (saveErr) {
                return res.status(400)
                    .send(saveErr);
            }
            getCategories(req, res);
        });
    });
}

export function updateTypeTranslations(req, res) {
    Type.update({ _id: req.params.typeId }, {
        $pull: { translations: { language: req.body.language } },
    }, { multi: true, safe: false }, (removeErr, rowsAffected) => {
        console.log('typeRowsAffected1: ', rowsAffected);
        if (removeErr) {
            return res.status(400)
                .send({ error: removeErr });
        } else {
            Type.update({ _id: req.params.typeId }, {
                $push: { translations: req.body },
            }, (pushErr, rowsAffected2) => {
                console.log('typeRowsAffected2: ', rowsAffected2);
                if (pushErr) {
                    return res.status(400)
                        .send({ error: pushErr });
                } else {
                    getCategories(req, res);
                }
            });
        }
    });
}

export function deleteType(req, res) {
    const type = req.type;
    // console.log("TYPE:",type);
    if (!!type._id) {
        type.remove((err) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            } else {
                getCategories(req, res);
            }
        });
    } else {
        getCategories(req, res);
    }
}
