import nodemailer from 'nodemailer';
import config from '../config';
import path from 'path';
import swig from 'swig-templates';
import Contact from '../models/contact';
import Service from '../models/service';
import Club from '../models/club';
import sanitizeHtml from 'sanitize-html';
import slug from 'limax';

const smtpTransport = nodemailer.createTransport(config.mailer.options);


export function testEmail(req, res) {
    const options = {
        to: 'bghack@gmail.com',
        subject: 'Testing Email System',
        data: 'Testing Email System on NodeJS.',
    };
    const template = swig.compileFile(path.resolve('./server/templates/emails/test-email.html'));
    const emailHTML = template({
        name: req.user.displayName,
        appName: config.app.title,
        title: options.subject,
        data: options.data,
    });
    const mailOptions = {
        to: options.to,
        from: config.mailer.from,
        subject: options.subject,
        html: emailHTML,
    };
    // console.log(emailHTML, mailOptions);
    smtpTransport.verify(function (error, success) {
        if (error) {
            console.log(error);
            return res.status(400)
                .send(error);
        } else {
            console.log('Server is ready to take our messages', success);
            return res.status(200)
                .send('Server is ready to take our messages');
        }
    });
    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            console.log(err);
            return res.status(400)
                .send({
                    error: 'Failure sending email',
                });
        }
        res.json({
            message: 'An email has been sent to the provided email with further instructions.',
        });
    });
}

export function contactProvider(req, res) {
    // Store record of the user contact form
    const newContact = new Contact();
    const isAnon = !req.user && req.body.anonymous;
    const user = isAnon ? req.body.email : req.user;
    const name = isAnon ? `${req.body.firstName} ${req.body.lastName}` : req.user.displayName;
    const email = isAnon ? user : req.user.email;

    newContact.user = isAnon ? undefined : user._id;
    newContact.email = email;
    newContact.provider = req.body.provider._id;
    // Let's sanitize inputs
    newContact.subject = req.body.subject;
    newContact.message = req.body.message;
    if (!isAnon) {
        newContact.details.yearOfBirth = req.body.yearOfBirth;
        newContact.details.streetAddress = req.body.street;
        newContact.details.postalCode = req.body.postalCode;
    }
    newContact.reason = req.body.reason;
    newContact.service = req.body.service._id;

    newContact.save((err, saved) => {
        if (err) {
            res.status(400)
                .send(err);
        }
    });
    //
    const options = {
        to: req.body.provider.email,
        subject: req.body.subject,
        cc: `admin@ageweb.care;${email};`,
    };
    const template = swig.compileFile(path.resolve('./server/templates/emails/contact-provider-email.html'));
    const emailHTML = template({
        appName: config.app.title,
        reason: req.body.reason,
        subject: options.subject,
        message: req.body.message,
        fromEmail: email,
        fromName: name,
    });
    const mailOptions = {
        to: options.to,
        from: config.mailer.from,
        subject: options.subject,
        // cc: options.cc,
        html: emailHTML,
    };
    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            console.log(err);
            return res.status(400)
                .send({
                    error: 'Failure sending email',
                });
        }
        res.json({
            message: 'An email has been sent to the provided email with further instructions.',
        });
    });
}

export function contactClub(req, res) {
    // Store record of the user contact form
    const newContact = new Contact();
    const isAnon = !req.user && req.body.anonymous;
    const user = isAnon ? req.body.email : req.user;
    const name = isAnon ? `${req.body.firstName} ${req.body.lastName}` : req.user.displayName;
    const email = isAnon ? user : req.user.email;

    newContact.user = isAnon ? undefined : user._id;
    newContact.email = email;
    newContact.provider = req.body.provider._id;
    // Let's sanitize inputs
    newContact.subject = req.body.subject;
    newContact.message = req.body.message;
    newContact.details.yearOfBirth = req.body.yearOfBirth;
    newContact.details.streetAddress = req.body.street;
    newContact.details.postalCode = req.body.postalCode;
    newContact.reason = req.body.reason;
    newContact.club = req.body.club._id;

    newContact.save((err, saved) => {
        if (err) {
            res.status(400)
                .send(err);
        }
    });
    //
    const options = {
        to: req.body.provider.email,
        subject: req.body.subject,
        cc: `admin@ageweb.care;${email};`,
    };
    const template = swig.compileFile(path.resolve('./server/templates/emails/contact-club-email.html'));
    const emailHTML = template({
        appName: config.app.title,
        reason: req.body.reason,
        subject: options.subject,
        message: req.body.message,
        fromEmail: email,
        fromName: name,
        postal: req.body.postalCode,
        address: req.body.street,
        year: req.body.yearOfBirth,
    });
    const mailOptions = {
        to: options.to,
        from: config.mailer.from,
        subject: options.subject,
        // cc: options.cc,
        html: emailHTML,
    };
    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            console.log(err);
            return res.status(400)
                .send({
                    error: 'Failure sending email',
                });
        }
        res.json({
            message: 'An email has been sent to the provided email with further instructions.',
        });
    });
}

export function contactAdmin(req, res) {
    // Store record of the user contact form
    const newContact = new Contact();
    const isAnon = req.body.anonymous;
    const user = isAnon ? req.body.email : req.user;

    newContact.user = isAnon ? undefined : user._id;
    newContact.email = isAnon ? user : user.email;
    newContact.provider = req.body.provider._id;
    // Let's sanitize inputs
    newContact.subject = sanitizeHtml(req.body.subject);
    newContact.message = sanitizeHtml(req.body.message);
    newContact.reason = req.body.reason;
    newContact.club = req.body.club._id;

    newContact.save((err, saved) => {
        if (err) {
            res.status(400)
                .send(err);
        }
    });
    //
    const options = {
        to: req.body.provider.email,
        subject: req.body.subject,
    };
    const template = swig.compileFile(path.resolve('./server/templates/emails/contact-provider-email.html'));
    const emailHTML = template({
        appName: config.app.title,
        reason: req.body.reason,
        subject: options.subject,
        message: req.body.message,
        fromEmail: isAnon ? user : user.email,
        fromName: isAnon ? user : user.displayName,
    });
    const mailOptions = {
        to: options.to,
        from: config.mailer.from,
        subject: options.subject,
        cc: options.cc,
        html: emailHTML,
    };
    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) {
            console.log(err);
            return res.status(400)
                .send({
                    error: 'Failure sending email',
                });
        }
        res.json({
            message: 'An email has been sent to the provided email with further instructions.',
        });
    });
}

export function invitationEmail(options, req, res, next) {
    const template = swig.compileFile(path.resolve('./server/templates/emails/invite-user-email.html'));
    const emailHTML = template({
        name: req.user.displayName,
        appName: config.app.title,
        username: options.username,
        password: options.password,
        email: options.email,
    });
    const mailOptions = {
        to: options.email,
        from: config.mailer.from,
        subject: 'AgeWeb User Invitation',
        html: emailHTML,
    };
    smtpTransport.sendMail(mailOptions, (err2) => {
        if (!err2) {
            next();
        } else {
            next(err2);
        }
    });
}

export function guestOrderInvitationEmail(options, req, res, next) {
    const template = swig.compileFile(path.resolve('./server/templates/emails/guest-user-order-invite-email.html'));
    const emailHTML = template({
        name: req.user.displayName,
        appName: config.app.title,
        username: options.username,
        password: options.password,
        email: options.email,
    });
    const mailOptions = {
        to: options.email,
        from: config.mailer.from,
        subject: 'AgeWeb User Invitation',
        html: emailHTML,
    };
    smtpTransport.sendMail(mailOptions, (err2) => {
        if (!err2) {
            next();
        } else {
            next(err2);
        }
    });
}

export function resetPasswordEmail(options, req, res, done) {
    res.render(path.resolve('./server/templates/emails/reset-password-email.html'), {
        name: options.user.displayName,
        token: options.token,
        appName: config.app.title,
        url: `${req.protocol + req.headers.host}/api/auth/reset/${options.token}`,
    }, (err, emailHTML) => {
        const mailOptions = {
            to: options.user.email,
            from: config.mailer.from,
            subject: 'Password Reset',
            html: emailHTML,
        };
        smtpTransport.sendMail(mailOptions, (err2) => {
            if (!err) {
                res.json({
                    message: 'An email has been sent to the provided email with further instructions.',
                });
            } else {
                return res.status(400)
                    .send({
                        error: 'Failure sending email',
                    });
            }
            done(err2, 'done');
        });
    });
}

export function resetPasswordConfirmEmail(options, req, res, done) {
    res.render('./server/templates/emails/reset-password-confirm-email.html', {
        name: options.user.displayName,
        appName: config.app.title,
    }, (err, emailHTML) => {
        const mailOptions = {
            to: options.user.email,
            from: config.mailer.from,
            subject: 'Your password has been changed',
            html: emailHTML,
        };

        smtpTransport.sendMail(mailOptions, (err2) => {
            if (!err2) {
                res.json({
                    message: 'An email has been sent to the provided email for password change confirmation.',
                });
            } else {
                res.status(400)
                    .send({
                        error: 'Failure sending email',
                    });
            }
            done(err2, 'done');
        });
    });
}

