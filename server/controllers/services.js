import fs from 'fs';
import mongoose from 'mongoose';
import Service from '../models/service';
import News from '../models/news';
import Category from '../models/category';
import Config from '../config';
import multer from 'multer';

import path from 'path';
import { translate } from './languages';

const _ = require('lodash');

export function searchServices(req, res) {
    let searchParams = {};
    if (req.body.hasOwnProperty('category') && req.body.category.length > 0) {
        searchParams.category = req.body.category;
    }
    if (req.body.hasOwnProperty('type') && req.body.type.length > 0) {
        searchParams.type = req.body.type;
    }
    if (req.body.hasOwnProperty('tag') && req.body.tag.length > 0) {
        searchParams.$text = { $search: req.body.tag };
    }
    let response = {};
    // console.log(req.body, searchParams);
    Service.find(searchParams)
        .exec((err, services) => {
            if (err) {
                // console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                // console.log(services);
                response.services = services;
                News.find(searchParams)
                    .exec((err2, news) => {
                        if (err2) {
                            // console.log(err2);
                            return res.status(400)
                                .send({ error: err2.message });
                        } else {
                            // console.log(news);
                            response.news = news;
                            return res.json(response);
                        }
                    });
            }
        });
}

/**
 * Create a event
 */
export function create(req, res) {
    delete req.body.image;
    let newService = new Service(req.body);
    newService.user = req.user._id;
    Service.create(newService, (err, service) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            Service.findById(service._id)
                .exec((err2, populatedService) => {
                    res.json(populatedService);
                });
        }
    });
}

/**
 * Show the current service
 */
export function read(req, res) {
    Service
        .findOne(req.service._id)
        .populate('user')
        .exec((err, service) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            res.json(service);
        });
}

/**
 * Get services for a producer
 */
export function getProviderServices(req, res) {
    Service.find({ user: { _id: req.user.userId } })
        .exec((err, services) => {
            if (err) {
                // console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else if (!services) {
                console.log('No service with that ID has been found');
                return res.status(404)
                    .send({ error: 'No service with that ID has been found' });
            } else {
                // console.log(services);
                res.json(services);
            }
        });
}


/**
 * Update a service
 */
export function update(req, res) {
    delete req.service.image;
    const service = _.extend(req.service, req.body);
    service.updated = Date.now();
    service.save((err) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json(service);
        }
    });
}

/**
 * Delete an service
 *
 */
export function deleteService(req, res) {
    const service = req.service;
    if (service.image && service.image !== '/api/static/img/default-image.png') {
        deleteServicePictures(service.image);
    }
    service.remove((err) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json(service);
        }
    });
}

/**
 * List of Services
 */
export function list(req, res) {
    Service.find({})
        .sort('-created')
        .exec((err, services) => {
            if (err) {
                // console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(services);
            }
        });
}

/**
 * List of Services by Type
 */
export function listByCategory(req, res) {
    Service.find({ category: req.category._id })
        .sort('-created')
        .exec((err, services) => {
            if (err) {
                // console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(services);
            }
        });
}

/**
 * List of Services by Type
 */
export function listCurrentByCategory(req, res) {
    const user = req.user;
    const serviceMatches = [{
        $match: {
            services: {
                $not: { $size: 0 },
            },
        },
    }];
    if (user && user.settings.country !== '') {
        const country = user.settings.country;
        /* serviceMatches.push({
            $match: {
                'services.countries': country,
            },
        }); */
        /* serviceMatches.push({
            $match: {
                'services.place.country': country,
            },
        }); */
    }
    if (user && user.settings.language) {
        const language = user.settings.language;
        /* serviceMatches.push({
            $match: {
                'services.language': language,
            },
        }); */
    }
    Category.aggregate([
        {
            $lookup: {
                from: 'services',
                localField: '_id',
                foreignField: 'category',
                as: 'services',
            },
        }].concat(serviceMatches))
        .exec((err, servicesByCats) => {
            if (err) {
                // console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                servicesByCats = translate(req.user, servicesByCats);
                res.json(servicesByCats);
            }
        });
}

/**
 * List of Services by Type
 */
export function listAllByCategory(req, res) {
    Category.aggregate(
        {
            $lookup:
                {
                    from: 'services',
                    localField: '_id',
                    foreignField: 'category',
                    as: 'services',
                },
        })
        .sort('-created')
        .exec((err, servicesByCats) => {
            if (err) {
                // console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(servicesByCats);
            }
        });
}

/**
 Helper functions
 */
const deleteServicePictures = (filePath) => {
    try {
        console.log(filePath);
        filePath = filePath.substr(filePath.lastIndexOf('/') + 1, filePath.length);
        console.log(filePath);
        fs.unlinkSync(Config.uploads.serviceUpload.dest + filePath, (err) => {
            if (err) {
                // console.log(err);
                return {
                    error: true,
                    message: err,
                };
            } else {
                return {
                    error: false,
                    message: `Successfully deleted : ${filePath}`,
                };
            }
        });
    } catch (e) {
        return e;
    }
};

/**
 * Change profile picture
 */
export function removeServicePicture(req, res) {
    let imgPath = req.body.filename;
    deleteServicePictures(imgPath);
    const service = req.service;
    /* let images = [].concat(service.images);
    images = _.remove(images,(img) => {
        return _.indexOf(img,imgName) > -1;
    }); */
    delete service.image;
    service.save((saveError, savedService) => {
        if (saveError) {
            return res.status(400)
                .send({ error: saveError });
        } else {
            // console.log('Removed picture from EVENT');
            res.json(savedService.image);
        }
    });
}

export function addServicePicture(req, res) {
    const service = req.service;
    // let message = null;
    const storage = multer.diskStorage({
        destination: (destReq, file, cb) => {
            cb(null, Config.uploads.serviceUpload.dest);
        },
        filename: (fileReq, file, cb) => {
            cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
        },
        limit: Config.uploads.serviceUpload.limits,
    });
    const upload = new multer({ storage }).single('service');
    // Filtering to upload only images
    upload(req, res, (uploadError) => {
        if (!req.file) {
            return res.status(400)
                .send({
                    message: 'No file to received.',
                });
        } else if (uploadError) {
            return res.status(400)
                .send({
                    message: 'Error occurred while uploading profile picture',
                    error: uploadError,
                });
        } else {
            const file = '/api' + req.file.destination.substr(1, req.file.destination.length) + req.file.filename;
            if (service) {
                service.image = file;
                service.save((saveError, savedService) => {
                    if (saveError) {
                        return res.status(400)
                            .send({ error: saveError });
                    } else {
                        // console.log('Image saved.');
                        res.json(savedService.image);
                    }
                });
            } else {
                // console.log('Save picture to NEW service');
                res.status(200)
                    .send(file);
            }
        }
    });
}

/**
 * Service middleware
 */
export function serviceByID(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400)
            .send({ error: 'ServiceID is invalid' });
    }
    Service.findById(id, (err, service) => {
        if (err) {
            next(err);
        } else if (!service) {
            // console.log('No service with that ID has been found');
            return res.status(404)
                .send({ error: 'No service with that ID has been found' });
        }
        req.service = service;
        next();
    });
}
