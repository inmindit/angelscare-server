import mongoose from 'mongoose';
import Comment from '../models/comment';
import Service from '../models/service';

/**
 * Function update service comment
 */
export function commentById(req, res, next, id) {
    Comment.findById(id, (err, com) => {
        if (err) {
            next(err);
        } else if (!com) {
            return res.status(404)
                .send({ error: 'No service with that ID has been found' });
        }
        req.comment = com;
        next();
    });
}

export function getServiceComments(req, res) {
    const service = req.service;
    Comment.find({ parent: service._id })
        .exec((err, comments) => {
            if (err) {
                return res.status(400)
                    .send(err);
            }
            res.json(comments);
        });
}


export function getPostComments(req, res) {
    const post = req.post;
    Comment.find({ parent: post._id })
        .exec((err, comments) => {
            if (err) {
                return res.status(400)
                    .send(err);
            }
            res.json(comments);
        });
}

export function addServiceComment(req, res) {
    const service = req.service;
    const comment = new Comment({
        text: req.body.text,
        user: req.body.user,
        parent: service,
    });
    comment.save((err, updated) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json(updated);
        }
    });
}

export function addPostComment(req, res) {
    const post = req.post;
    const comment = new Comment({
        text: req.body.text,
        user: req.body.user,
        parent: post,
    });
    comment.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send(err);
        }
        res.json(saved);
    });
}

export function updateComment(req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.body._id)) {
        return res.status(400)
            .send({ error: 'Comment is invalid' });
    }
    Comment.findById(req.body._id, (err, comment) => {
        comment.set({ text: req.body.text });
        comment.save((saveErr, saved) => {
            if (saveErr) {
                return res.status(400).send(saveErr);
            }
            res.json(saved);
        });
    });
}

export function deleteComment(req, res) {
    const comment = req.comment;
    comment.remove((err) => {
        if (err) {
            res.status(400)
                .send({ error: err.message });
        } else {
            res.json(comment);
        }
    });
}
