import Club from '../models/club';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';
import mongoose from 'mongoose';
import Config from '../config';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import _ from 'lodash';
import News from '../models/news';

/**
 * Get all posts
 * @param req
 * @param res
 * @returns void
 */
export function getAllClubs(req, res) {
    Club.find()
        .sort('-dateAdded')
        .exec((err, posts) => {
            if (err) {
                return res.status(400)
                    .send({ error: err.message });
            }
            res.json(posts);
        });
}

/**
 * List of Clubs by Category
 */
export function getClubsByCategory(req, res) {
    Club.find({ category: req.category._id })
        .sort('-created')
        .exec((err, news) => {
            if (err) {
                console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(news);
            }
        });
}
/**
 * List of Clubs by Type
 */
export function getClubsByType(req, res) {
    Club.find({ type: req.type._id })
        .sort('-created')
        .exec((err, news) => {
            if (err) {
                console.log(err);
                return res.status(400)
                    .send({ error: err.message });
            } else {
                res.json(news);
            }
        });
}

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function addClub(req, res) {
    if (!req.body.title || !req.body.content) {
        res.status(403)
            .send({ error: 'Club article must have Title and Content.' });
    }

    delete req.body.image;
    const newClub = new Club(req.body);

    newClub.user = req.user._id;
    // Let's sanitize inputs
    newClub.title = sanitizeHtml(newClub.title);
    newClub.content = sanitizeHtml(newClub.content);

    newClub.slug = slug(newClub.title.toLowerCase(), { lowercase: true });
    newClub.save((err, saved) => {
        if (err) {
            res.status(400)
                .send(err);
        }
        res.json(saved);
    });
}

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
export function updateClub(req, res) {
    delete req.body.image;
    const club = _.extend(req.club, req.body);
    club.updated = Date.now();
    club.save((err, saved) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        } else {
            res.json(saved);
        }
    });
}

/**
 * Get a single club
 * @param req
 * @param res
 * @returns void
 */
export function getClub(req, res) {
    Club.findOne({ _id: req.params.clubId }).populate('user')
        .exec((err, club) => {
            if (err) {
                res.status(400)
                    .send({ error: err.message });
            }
            res.json(club);
        });
}

/**
 * Delete a club
 * @param req
 * @param res
 * @returns void
 */
export function deleteClub(req, res) {
    Club.findOne({ _id: req.params.clubId }, (err, club) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        }

        club.remove(() => {
            res.status(200)
                .end();
        });
    });
}

/**
 Helper functions
 */
const deleteClubPictures = (filePath) => {
    try {
        console.log(filePath);
        filePath = filePath.substr(filePath.lastIndexOf('/') + 1, filePath.length);
        console.log(filePath);
        fs.unlinkSync(Config.uploads.serviceUpload.dest + filePath, (err) => {
            if (err) {
                console.log(err);
                return {
                    error: true,
                    message: err,
                };
            } else {
                return {
                    error: false,
                    message: `Successfully deleted : ${filePath}`,
                };
            }
        });
    } catch (e) {
        return e;
    }
};

/**
 * Change profile picture
 */
export function removeClubPicture(req, res) {
    const imgPath = req.body.filename;
    deleteClubPictures(imgPath);
    const club = req.club;
    /* let images = [].concat(club.images);
    images = _.remove(images,(img) => {
        return _.indexOf(img,imgName) > -1;
    }); */
    delete club.image;
    club.save((saveError, savedService) => {
        if (saveError) {
            return res.status(400)
                .send({ error: saveError });
        } else {
            console.log('Removed picture from EVENT');
            res.json(savedService.image);
        }
    });
}

export function addClubPicture(req, res) {
    const club = req.club;
    // let message = null;
    const storage = multer.diskStorage({
        destination: (destReq, file, cb) => {
            cb(null, Config.uploads.clubUpload.dest);
        },
        filename: (fileReq, file, cb) => {
            cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
        },
        limit: Config.uploads.clubUpload.limits,
    });
    const upload = new multer({ storage }).single('clubs');
    // Filtering to upload only images
    upload(req, res, (uploadError) => {
        if (!req.file) {
            return res.status(400)
                .send({
                    message: 'No file to received.',
                });
        } else if (uploadError) {
            return res.status(400)
                .send({
                    message: 'Error occurred while uploading profile picture',
                    error: uploadError,
                });
        } else {
            const file = '/api' + req.file.destination.substr(1, req.file.destination.length) + req.file.filename;
            if (club) {
                club.image = file;
                club.save((saveError, savedService) => {
                    if (saveError) {
                        return res.status(400)
                            .send({ error: saveError });
                    } else {
                        console.log('Image saved.');
                        res.json(savedService.image);
                    }
                });
            } else {
                console.log('Save picture to NEW club');
                res.status(200)
                    .send(file);
            }
        }
    });
}

/**
 * Function to populate club if clubId is available
 * @param req
 * @param res
 * @param next
 * @param id
 * @returns void
 */
export function clubByID(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400)
            .send({ error: 'ClubID is invalid' });
    }
    Club.findById(id)
        .populate('user', 'displayName')
        .exec((err, club) => {
            if (err) {
                return next(err);
            } else if (!club) {
                return res.status(404)
                    .send({ error: 'No Club with that identifier has been found' });
            }
            req.club = club;
            next();
        });
}
