import _ from 'lodash';
import fs from 'fs';
import mongoose from 'mongoose';
import User from '../models/user';
import Service from '../models/service';
import Field from '../models/event_fields';
import Notification from '../models/notification';

Service.pre('create', (next) => {
    User.find({ roles: ['user', 'provider'] })
        .exec((err, users) => {
            const userUsers = _.findWhere(users, (user) => {
                return _.some(user.roles, (role) => {
                    return role === 'user';
                });
            });

            _.each(userUsers, (user) => {
                Notification.create({
                    user,
                    event,
                    title: 'Има ново събитие',
                });
            });
            next();
        });
});

