import config from '../config';
import Payment from '../models/payment';
import User from '../models/user';
import Order from '../models/order';
import paypal from 'paypal-rest-sdk';

paypal.configure(config.api);

export function isUserExists(data, callback) {
    const response = {};
    User.findOne(data, (err, result) => {
        if (result != null) {
            response.process = 'success';
            response.isUserExists = true;
            response.id = result._id;
            response.name = result.name;
        } else {
            response.process = 'failed';
            response.isUserExists = false;
        }
        callback(response);
    });
}

export function insertPayment(data, callback) {
    const response = {};
    Payment.insertOne(data, (err, result) => {
        if (err) {
            response.isPaymentAdded = false;
            response.message = 'Something went Wrong,try after sometime.';
        } else {
            response.isPaymentAdded = true;
            response.id = result.ops[0]._id;
            response.message = 'Payment added.';
        }
        callback(response);
    });
}

export function getProductInfo(data, callback) {
    const response = {};
    Order.findOne(data, (err, result) => {
        if (result != null) {
            response.error = false;
            response.data = result;
        } else {
            response.error = true;
        }
        callback(response);
    });
}

export function getAllProducts(data, callback) {
    Order.find()
        .toArray((err, result) => {
            callback(result);
            db.close();
        });
}

export function payNow(paymentData, callback) {
    const response = {};

    /* Creating Payment JSON for Paypal starts */
    const payment = {
        intent: 'authorize',
        payer: {
            payment_method: 'paypal',
        },
        redirect_urls: {
            return_url: 'http://127.0.0.1:81/api/execute',
            cancel_url: 'http://127.0.0.1:81/api/cancel',
        },
        transactions: [{
            amount: {
                total: paymentData.data.price,
                currency: 'USD',
            },
            description: paymentData.data.productName,
        }],
    };
    /* Creating Payment JSON for Paypal ends */

    /* Creating Paypal Payment for Paypal starts */
    paypal.payment.create(payment, (error, resPayment) => {
        if (error) {
            console.log(error);
        } else {
            if (resPayment.payer.payment_method === 'paypal') {
                response.paymentId = resPayment.id;
                let redirectUrl;
                response.payment = resPayment;
                for (let i = 0; i < resPayment.links.length; i++) {
                    const link = resPayment.links[i];
                    if (link.method === 'REDIRECT') {
                        redirectUrl = link.href;
                    }
                }
                response.redirectUrl = redirectUrl;
            }
        }
        /*
        * Sending Back Paypal Payment response
        */
        callback(error, response);
    });
    /* Creating Paypal Payment for Paypal ends */
}

export function getResponse(data, PayerID, callback) {
    const response = {};

    const serverAmount = parseFloat(data.paypalData.payment.transactions[0].amount.total);
    const clientAmount = parseFloat(data.clientData.price);
    const paymentId = data.paypalData.paymentId;
    const details = {
        payer_id: PayerID,
    };

    response.userData = {
        userID: data.sessionData.userID,
        name: data.sessionData.name,
    };

    if (serverAmount !== clientAmount) {
        response.error = true;
        response.message = 'Payment amount doesn\'t matched.';
        callback(response);
    } else {
        paypal.payment.execute(paymentId, details, (error, payment) => {
            if (error) {
                console.log(error);
                response.error = false;
                response.message = 'Payment Successful.';
                callback(response);
            } else {
                /*
                * inserting paypal Payment in DB
                */

                const newPayment = {
                    userId: data.sessionData.userID,
                    paymentId,
                    createTime: payment.create_time,
                    state: payment.state,
                    currency: 'USD',
                    amount: serverAmount,
                    createAt: new Date().toISOString(),
                };

                self.insertPayment(newPayment, (result) => {
                    if (!result.isPaymentAdded) {
                        response.error = true;
                        response.message = 'Payment Successful, but not stored.';
                        callback(response);
                    } else {
                        response.error = false;
                        response.message = 'Payment Successful.';
                        callback(response);
                    }
                });
            }
        });
    }
}
