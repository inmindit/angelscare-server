/**
 * Module dependencies
 */
import https from 'https';
import _ from 'lodash';
import multer from 'multer';
import path from 'path';
import config from '../../config';
import Language from '../../models/language';
import User from '../../models/user';


export function safeUserObject(user) {
    let userObj = {};
    if (!!user) {
        userObj = {
            id: user._id,
            displayName: user.displayName,
            username: user.username,
            created: user.created,
            roles: user.roles,
            profileImageURL: user.profileImageURL,
            email: user.email,
            lastName: user.lastName,
            firstName: user.firstName,
            location: user.location,
            fullLocation: user.fullLocation,
            phone: user.phone,
            access: user.access,
            status: user.status,
            settings: user.settings,
        };
    }
    return userObj;
}

function findLanguage(user, cb, tag) {
    if (!tag) {
        tag = '';
        if (user.settings.country === 'Bulgaria') {
            tag = 'BG';
        } else if (user.settings.country === 'Germany' || user.settings.country === 'Austria' || user.settings.country === 'Belgium' || user.settings.country === 'Switzerland') {
            tag = 'DE';
        } else {
            tag = 'EN';
        }
    }
    Language.findOne({ tag }, (langErr, lang) => {
        if (!langErr) {
            user.settings.language = lang._id;
            console.log('Selected Language: ', lang);
            cb(user);
        }
    });
}

/**
 * Update user language preference
 */
export function updateUserSetting(req, res) {
    // Init Variables
    const user = req.user;
    const updateUser = (newUser) => {
        newUser.updated = Date.now();
        newUser.save((err) => {
            if (err) {
                res.status(400)
                    .send({ error: err });
            } else {
                User.findOne({ _id: newUser._id })
                    .populate('settings.language', 'access')
                    .exec((userErr, populatedUser) => {
                        if (userErr) {
                            res.status(401)
                                .send({ error: userErr });
                        } else {
                            res.json(safeUserObject(populatedUser));
                        }
                    });
            }
        });
    };
    if (user) {
        // Merge existing user
        if (req.body.language) {
            findLanguage(user, updateUser, req.body.language.toUpperCase());
        } else if (req.body.country) {
            user.settings.country = req.body.country;
            findLanguage(user, updateUser);
        } else if (req.body.place) {
            user.location = req.body.place.location;
            delete req.body.place.location;
            user.fullLocation = req.body.place;
            user.settings.country = req.body.place.country;
            // Language Check and Set
            findLanguage(user, updateUser);
        }
        console.log(req.body);
    } else {
        return res.status(401)
            .send({ error: 'User must be signed in to update his language.' });
    }
}


/**
 * Update user details
 */
export function update(req, res) {
    // Init Variables
    let user = req.user;

    // For security measurement we remove the roles from the req.body object
    const allowed = ['username', 'roles', 'access', 'profileImageURL', 'password', 'salt', 'displayName', 'created', 'updated',
        'resetPasswordToken', 'resetPasswordExpires', 'provider', 'providerData', 'additionalProvidersData'];
    let newUser = req.body;
    // console.log('BEFORE: ', newUser);
    newUser = _.omit(newUser, allowed);
    // console.log('AFTER: ', newUser);
    if (user) {
        // Merge existing user
        user = _.extend(user, newUser);
        user.updated = Date.now();
        // user.displayName = `${user.firstName} ${user.lastName}`;

        user.save((err) => {
            if (err) {
                res.status(400)
                    .send({ error: err });
            } else {
                req.login(user, (err2) => {
                    if (err2) {
                        res.status(401)
                            .send({ error: err2 });
                    } else {
                        res.json(safeUserObject(user));
                    }
                });
            }
        });
    } else {
        res.status(401)
            .send({ error: 'User is not signed in' });
    }
}

/**
 * Update profile picture
 */
export function changeProfilePicture(req, res) {
    const user = req.user;

    if (user) {
        const storage = multer.diskStorage({
            destination: (destReq, file, cb) => {
                cb(null, config.uploads.profileUpload.dest);
            },
            filename: (fileReq, file, cb) => {
                cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
            },
            limit: config.uploads.profileUpload.limits,
        });
        const upload = new multer({ storage: storage }).single('profile');
        upload(req, res, (uploadError) => {
            if (uploadError) {
                res.status(400)
                    .send({ error: 'Error occurred while uploading profile picture' });
            } else {
                user.profileImageURL = '/api' + req.file.destination.substr(1, req.file.destination.length) + req.file.filename;

                user.save((saveError) => {
                    if (saveError) {
                        res.status(400)
                            .send({ error: saveError });
                    } else {
                        res.json(safeUserObject(user));
                    }
                });
            }
        });
    } else {
        res.status(401)
            .send({
                error: 'User is not signed in',
            });
    }
}

/**
 * Send User
 */
export function me(req, res) {
    // console.log(req.user, req.sessionID);
    res.json(safeUserObject(req.user) || null);
}
