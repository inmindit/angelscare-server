/**
 * Module dependencies
 */
import mongoose from 'mongoose';
import User from '../../models/user';

/**
 * User middleware
 */
export function userByID(req, res, next, id) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400)
            .send({error:'User is invalid'});
    }
    if (!!id) {
        User.findOne({
            _id: id,
        }).exec((err, user) => {
                if (err) {
                    next({error:err});
                } else if (!user) {
                    next({error:new Error(`Failed to load User ${id}`)});
                }
                req.profile = user;
                next();
            });
    }
    next(new Error(`Failed to load User ${id}`));
}
