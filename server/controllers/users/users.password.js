/**
 * Module dependencies
 */
import User from '../../models/user';
import async from 'async';
import crypto from 'crypto';
import { resetPasswordEmail, resetPasswordConfirmEmail } from '../contact';

/**
 * Forgot for reset password (forgot POST)
 */
export function forgot(req, res, next) {
    async.waterfall([
        // Generate random token
        (done) => {
            crypto.randomBytes(20, (err, buffer) => {
                const token = buffer.toString('hex');
                done({ error: err }, token);
            });
        },
        // Lookup user by username
        (token, done) => {
            if (req.body.username) {
                User.findOne({
                    username: req.body.username.toLowerCase(),
                }, '-salt -password', (err, user) => {
                    if (err || !user) {
                        res.status(400)
                            .send({ error: 'No account with that username has been found' });
                    } else if (user.provider !== 'local') {
                        res.status(400)
                            .send({ error: `It seems like you signed up using your ${user.provider} account` });
                    } else {
                        user.resetPasswordToken = token;
                        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                        user.save((err2) => {
                            done({ error: err2 }, token, user);
                        });
                    }
                });
            } else {
                res.status(400)
                    .send({ error: 'Username field must not be blank' });
            }
        },
        (token, user, done) => {
            resetPasswordEmail({
                token,
                user,
            }, req, res, done);
        },
    ], (err) => {
        if (err) {
            next(err);
        }
    });
}

/**
 * Reset password GET from email token
 */
export function validateResetToken(req, res) {
    User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {
            $gt: Date.now(),
        },
    }, (err, user) => {
        if (err || !user) {
            res.redirect('/password/reset/invalid');
        }

        res.redirect(`/password/reset/${req.params.token}`);
    });
}

/**
 * Reset password POST from email token
 */
export function reset(req, res, next) {
    // Init Variables
    const passwordDetails = req.body;

    async.waterfall([

        (done) => {
            User.findOne({
                resetPasswordToken: req.params.token,
                resetPasswordExpires: {
                    $gt: Date.now(),
                },
            }, (err, user) => {
                if (!err && user) {
                    if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                        user.password = passwordDetails.newPassword;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;

                        user.save((err2) => {
                            if (err2) {
                                res.status(400)
                                    .send({ error: err2 });
                            } else {
                                req.login(user, (err3) => {
                                    if (err) {
                                        res.status(401)
                                            .send({ error: err3 });
                                    } else {
                                        // Remove sensitive data before return authenticated user
                                        user.password = undefined;
                                        user.salt = undefined;

                                        res.json(user);

                                        done(err, user);
                                    }
                                });
                            }
                        });
                    } else {
                        res.status(400)
                            .send({ error: 'Passwords do not match' });
                    }
                } else {
                    res.status(400)
                        .send({ error: 'Password reset token is invalid or has expired.' });
                }
            });
        },
        (user, done) => {
            resetPasswordConfirmEmail({
                user,
            }, req, res, done);
        },
    ], (err) => {
        if (err) {
            next(err);
        }
    });
}

/**
 * Change Password
 */
export function changePassword(req, res, next) {
    // Init Variables
    const passwordDetails = req.body;

    if (req.user) {
        if (passwordDetails.newPassword) {
            User.findById(req.user.id, (err, user) => {
                if (!err && user) {
                    if (user.authenticate(passwordDetails.currentPassword)) {
                        if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                            user.password = passwordDetails.newPassword;

                            user.save((err2) => {
                                if (err2) {
                                    res.status(400)
                                        .send({ error: err2 });
                                } else {
                                    req.login(user, (err3) => {
                                        if (err3) {
                                            res.status(401)
                                                .send({ error: err3 });
                                        } else {
                                            res.status(200)
                                                .send({ error: 'Password changed successfully' });
                                        }
                                    });
                                }
                            });
                        } else {
                            res.status(400)
                                .send({ error: 'Passwords do not match' });
                        }
                    } else {
                        res.status(400)
                            .send({ error: 'Current password is incorrect' });
                    }
                } else {
                    res.status(400)
                        .send({ error: 'User is not found' });
                }
            });
        } else {
            res.status(400)
                .send({ error: 'Please provide a new password' });
        }
    } else {
        res.status(401)
            .send({ error: 'User is not signed in' });
    }
}
