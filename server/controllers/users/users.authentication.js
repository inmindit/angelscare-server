/* eslint-disable no-param-reassign */
import sanitizeHtml from 'sanitize-html';

/**
 * Module dependencies
 */
const passport = require('passport');
import User from '../../models/user';
import _ from 'lodash';

// URLs for which user can't be redirected on signin
const noReturnUrls = [
    '/authentication/signin',
    '/authentication/signup',
];

export function safeUserObject(user) {
    let userObj = {};
    if (!!user) {
        userObj = {
            id: user._id,
            displayName: user.displayName,
            username: user.username,
            created: user.created.toString(),
            roles: user.roles,
            profileImageURL: user.profileImageURL,
            email: user.email,
            lastName: user.lastName,
            firstName: user.firstName,
            location: user.location,
            fullLocation: user.fullLocation,
            phone: user.phone,
            access: user.access,
            status: user.status,
            settings: user.settings,
            sessionID: user.sessionID,
        };
    }
    return userObj;
}

/**
 * Signin after passport authentication
 */
export function signin(req, res, next) {
    passport.authenticate('local', (err, user, info) => {
        if (err || !user) {
            // console.log('PASSPORT ERROR: ',err,info);
            return res.status(401)
                .send({error:info});
        }
        // Remove sensitive data before login
        user.password = undefined;
        user.provider = undefined;
        user.salt = undefined;
        user.resetPasswordExpires = undefined;
        user.resetPasswordToken = undefined;

        req.login(user, (err2) => {
            if (err2) {
                // console.log("LOGIN ERROR: ",err);
                return res.status(400)
                    .send({error:"Wrong credentials."});
            }
            user.sessionID = req.sessionID;
            // console.log('Logging in :', user.username, req.sessionID);
            user = safeUserObject(user);
            res.json({ user });
        });
    })(req, res, next);
}

/**
 * Signup
 */
export function signup(req, res) {
    // For security measurement we remove the roles from the req.body object
    // console.log(req.body);
    // Init user and add missing fields
    const user = new User(req.body);
    delete req.body.roles;
    user.username = sanitizeHtml(req.body.username);
    user.password = sanitizeHtml(req.body.password);
    user.email = sanitizeHtml(req.body.email);
    user.firstName = sanitizeHtml(req.body.firstName);
    user.lastName = sanitizeHtml(req.body.lastName);
    user.provider = 'local';
    user.displayName = `${user.firstName} ${user.lastName}`;
    user.roles = ['user'];
    // console.log(user);
    // Then save the user
    user.save((err, saved) => {
        if (err && !saved) {
            // console.log(err);
            if (err.code === 11000)
                err = "User already exists in the System. Username or Email has already been Registered in the system. Have you forgotten your password ?";
            return res.status(400)
                .send({error:err});
        }
        // Remove sensitive data before login
        saved.password = undefined;
        saved.salt = undefined;
        //console.log(saved);
        //TODO: Send email confirmation for user.
        req.login(saved, (err2) => {
            if (err2) {
                //TODO Add exceptions for each status code from sMongo
                return res.status(400)
                    .send({error:err2});
            }
            user.sessionID = req.sessionID;
            // console.log('Logging in :', user.username, req.sessionID);
            saved = safeUserObject(saved);
            res.json({ user: saved });
        });
    });
}

/**
 * Signout
 */
export function signout(req, res) {
    req.logout();
    res.redirect('/');
}

/**
 * OAuth provider call
 */
export function oauthCall(strategy, scope) {
    return (req, res, next) => {
        // Set redirection path on session.
        // Do not redirect to a signin or signup page
        if (noReturnUrls.indexOf(req.query.redirect_to) === -1) {
            req.session.redirect_to = req.query.redirect_to;
        }
        // Authenticate
        passport.authenticate(strategy, scope)(req, res, next);
    };
}

/**
 * OAuth callback
 */
export function oauthCallback(strategy) {
    return (req, res, next) => {
        // Pop redirect URL from session
        const sessionRedirectURL = req.session.redirect_to;
        delete req.session.redirect_to;

        passport.authenticate(strategy, (err, user, redirectURL) => {
            if (err) {
                res.redirect(`/authentication/signin?err=${encodeURIComponent(err)}`);
            }
            if (!user) {
                res.redirect('/authentication/signin');
            }
            req.login(user, (err2) => {
                if (err2) {
                    res.redirect('/authentication/signin');
                }
                res.redirect(redirectURL || sessionRedirectURL || '/');
            });
        })(req, res, next);
    };
}

/**
 * Helper function to save or update a OAuth user profile
 */
export function saveOAuthUserProfile(req, providerUserProfile, done) {
    if (!req.user) {
        // Define a search query fields
        const searchMainProviderIdentifierField = `providerData.${providerUserProfile.providerIdentifierField}`;
        const searchAdditionalProviderIdentifierField = `additionalProvidersData.${providerUserProfile.provider}.${providerUserProfile.providerIdentifierField}`;

        // Define main provider search query
        const mainProviderSearchQuery = {};
        mainProviderSearchQuery.provider = providerUserProfile.provider;
        mainProviderSearchQuery[searchMainProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

        // Define additional provider search query
        const additionalProviderSearchQuery = {};
        additionalProviderSearchQuery[searchAdditionalProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];

        // Define a search query to find existing user with current provider profile
        const searchQuery = {
            $or: [mainProviderSearchQuery, additionalProviderSearchQuery],
        };

        User.findOne(searchQuery, (err, user) => {
            if (err) {
                done({error:err});
            } else {
                if (!user) {
                    const possibleUsername = providerUserProfile.username || ((providerUserProfile.email) ? providerUserProfile.email.split('@')[0] : '');

                    User.findUniqueUsername(possibleUsername, null, (availableUsername) => {
                        user = new User({
                            firstName: providerUserProfile.firstName,
                            lastName: providerUserProfile.lastName,
                            username: availableUsername,
                            displayName: providerUserProfile.displayName,
                            email: providerUserProfile.email,
                            profileImageURL: providerUserProfile.profileImageURL,
                            provider: providerUserProfile.provider,
                            providerData: providerUserProfile.providerData,
                        });

                        // And save the user
                        user.save((err2) => {
                            done(err2, user);
                        });
                    });
                } else {
                    done({error:err}, user);
                }
            }
        });
    } else {
        // User is already logged in, join the provider data to the existing user
        const user = req.user;

        // Check if user exists, is not signed in using this provider, and doesn't have that provider data already configured
        if (user.provider !== providerUserProfile.provider && (!user.additionalProvidersData || !user.additionalProvidersData[providerUserProfile.provider])) {
            // Add the provider data to the additional provider data field
            if (!user.additionalProvidersData) {
                user.additionalProvidersData = {};
            }

            user.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;

            // Then tell mongoose that we've updated the additionalProvidersData field
            user.markModified('additionalProvidersData');

            // And save the user
            user.save((err) => {
                done({error:err}, user, '/settings/accounts');
            });
        } else {
            done(new Error('User is already connected using this provider'), user);
        }
    }
}

/**
 * Remove OAuth provider
 */
export function removeOAuthProvider(req, res, next) {
    const user = req.user;
    const provider = req.query.provider;

    if (!user) {
        return res.status(401)
            .json({
                error: 'User is not authenticated.',
            });
    } else if (!provider) {
        return res.status(400)
            .send({
                error: "No provider available for removal."
            });
    }

    // Delete the additional provider
    if (user.additionalProvidersData[provider]) {
        delete user.additionalProvidersData[provider];

        // Then tell mongoose that we've updated the additionalProvidersData field
        user.markModified('additionalProvidersData');
    }

    user.save((err) => {
        if (err) {
            return res.status(400)
                .send({ error: err.message });
        }
        req.login(user, (err2) => {
            if (err2) {
                return res.status(400)
                    .send({ error: err2.message });
            }
            res.json(user);
        });
    });
}
