export default {
    locale: 'en',
    messages: {
        siteTitle: 'AgeWeb.CARE',
        addPost: 'Add Post',
        switchLanguage: 'Switch Language',
        by: 'By',
        deletePost: 'Delete Post',
        createNewPost: 'Create new post',
        authorName: 'Author\'s Name',
        postTitle: 'Post Title',
        postContent: 'Post Content',
        submit: 'Submit',
        nestedDateComment: `user {name} {value, plural,
    	  =0 {does not have any comments}
    	  =1 {has # comment}
    	  other {has # comments}
    	} as of {date}`,
        addService: 'Add service',
        serviceTitle: 'Title',
        serviceDescription: 'Description',
        createNewService: 'Create new service',
    },
};
