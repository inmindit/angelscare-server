export default {
    locale: 'bg',
    messages: {
        siteTitle: 'Tickets.IO - Билетна Платформа',
        addPost: 'Добави публикация',
        switchLanguage: 'Смени езика',
        twitterMessage: 'Намерете ни в Twitter.',
        by: 'от',
        deletePost: 'Изтрий публикация',
        createNewPost: 'Създай нова публикация',
        authorName: 'Автор',
        postTitle: 'Заглавие на публикацията',
        postContent: 'Съдържание',
        submit: 'Изпрати',
        comment: `Потребител {name} {value, plural,
    	  =0 {does not have any comments}
    	  =1 {has # comment}
    	  other {has # comments}
    	} (in real app this would be translated to Български)`,
        HTMLComment: `Потребител <b style='font-weight: bold'>{name} </b> {value, plural,
    	  =0 {does not have <i style='font-style: italic'>any</i> comments}
    	  =1 {has <i style='font-style: italic'>#</i> comment}
    	  other {has <i style='font-style: italic'>#</i> comments}
    	} (in real app this would be translated to Bulgarian)`,
        nestedDateComment: `Потребител {name} {value, plural,
  		  =0 {does not have any comments}
  		  =1 {has # comment}
  		  other {has # comments}
  		} as of {date} (in real app this would be translated to French)`,
    },
};
