![](http://ageweb.care/src/public/assets/img/logo_400.png)

# AgeWeb Elderly Services Provicer

AgeWeb is a custom project developed for providing services to eldery people in europe.

- [Website](http://ageweb.care)

## Quickstart

```
  npm install -g yarn
  yarn install
  yarn star
  # yarn run prod
```

**Note : Please make sure your MongoDB is running.** For MongoDB installation guide see [this](https://docs.mongodb.org/v3.0/installation/). Also `npm3` is required to install dependencies properly.

## Available Commands

1. `npm run start` - starts the development server with hot reloading enabled

2. `npm run prod` - starts the development server with hot reloading enabled

3. `npm run test` - start the test runner

4. `npm run lint` - runs linter to check for lint errors

### Webpack Configs
We use Webpack for bundling modules. There are two types of Webpack configs provided `webpack.config.dev.js` (for development), `webpack.config.prod.js` (for production).

For [babel-plugin-webpack-loaders](https://github.com/istarkov/babel-plugin-webpack-loaders) for server rendering of assets included through webpack).

The Webpack configuration is minimal and beginner-friendly. You can customise and add more features to it for production build.


### Importing Assets
Assets can be kept where you want and can be imported into your js files or css files. Those fill be served by webpack in development mode and copied to the dist folder during production.

### ES6 support
We use babel to transpile code in both server and client with `stage-0` plugin. So, you can use both ES6 and experimental ES7 features.

### Docker
There are docker configurations for both development and production.

To run docker for development,
```
docker-compose -f docker-compose-development.yml build
docker-compose -f docker-compose-development.yml up
```

To run docker for production,
```
docker-compose build
docker-compose up
```


## License
AgeWeb is a private project in development.
